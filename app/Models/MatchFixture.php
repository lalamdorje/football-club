<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MatchFixture extends Model
{
    use HasFactory;
    protected $table = 'matches';
    protected $fillable = ['opponent_team_id', 'date', 'goal_scored', 'goal_conceded', 'result', 'status'];

    public function opponent_team(){
        return $this->belongsTo(OpponentTeam::class);
    }
    public function played(){
        return MatchFixture::where('status', 1)->latest()->get();
    }
    public function notPlayed(){
        return MatchFixture::where('status', 0)->latest()->get();
    }

    public function ownResult(){
        $ourTable = $this->played()->collect()->groupBy('result')->map(function($result){
            return collect($result)->count();
        })->all();
        return $ourTable;
    }
    public function ownPoints(){
        $result = $this->ownResult();
        $points = ($result['wins'] ?? 0) * 3 + ($result['draws'] ?? 0) * 1 ;
        return $points;
    }

    public function ownPlayed(){
        return count($this->played());
    }

    public function ownStats(){
        $result = $this->ownResult();
        $result['matches_played'] = $this->ownPlayed();
        $result['points'] = $this->ownPoints();
        return $result;
    }
}
