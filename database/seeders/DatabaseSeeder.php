<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(AdminSeeder::class);
        $this->call(SiteSettingSeeder::class);
        $this->call(AboutSeeder::class);
        $this->call(PlayerRoleSeeder::class);
        $this->call(TeamSeeder::class);
        $this->call(ImageSliderSeeder::class);
        $this->call(NewsSeeder::class);
    }
}
