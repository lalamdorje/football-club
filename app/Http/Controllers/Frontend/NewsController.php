<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\News;

class NewsController extends Controller
{
    public function news(){
        $newses=News::where('status', 1)->get();
        $latestNews=News::where('status', 1)->latest()->take(4)->get();
        return view('frontend.news', compact('newses','latestNews'));
    }

    public function detail($id){
        $newsDetail = News::find($id);
        return view('frontend.news_detail', compact('newsDetail'));
    }
}
