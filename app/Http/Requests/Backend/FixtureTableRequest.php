<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class FixtureTableRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
            'opponent_team_id' => 'required|unique:fixture_tables',
            'wins' => 'required|integer',
            'draws' => 'required|integer',
            'losses' => 'required|integer',
            'points' => 'required|integer',
        ];
        if ($this->method() == 'PUT' || $this->method() == 'PATCH'){
            $rules['opponent_team_id'] = 'required';
        }
        return $rules;
    }
}
