<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\FixtureTable;
use App\Models\MatchFixture;
use App\Models\News;
use App\Models\SiteSetting;
use Illuminate\Http\Request;

class MatchController extends Controller
{
    public function index(){
        $siteSetting = SiteSetting::first();
        $matches = (new MatchFixture())->notPlayed();
        $completedMatches =  (new MatchFixture())->played();
        $latestNews = News::where('status', 1)->latest()->take(4)->get();
        $fixtureTables = FixtureTable::orderBy('points', 'desc')->get();
        $ourTable[0] = (new MatchFixture())->ownStats();
        $fixtureTables =  collect($ourTable)->merge($fixtureTables)->sortByDesc('points');;
        return view('frontend.match', compact('latestNews', 'matches', 'siteSetting', 'completedMatches', 'fixtureTables'));
    }
}
