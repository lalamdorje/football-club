@extends('frontend.layouts.app')
@section('content')
<section class="breadcrumbs" style="background-image: url('{{asset('frontend/assets/img/breadcrumb-bg.jpg')}}');">     
     <div class="container">
         <ol>
            <li><a href="{{route('frontend.index')}}">Home</a></li>
            <li>News Detail</li>
         </ol>
         <h2>News Detail</h2>
      </div>
   </section>
   <section class="blog-detail-section">
      <div class="container">
         <div class="row">
            <div class="col-md-10 offset-md-1">
               <div class="blog-detail-main">
                  <div class="blog-detail-head text-center">
                     <ul>
                        <li>
                           <i class='bx bx-calendar'></i> <span> {{ $newsDetail->created_at->format('d F, Y') }}</span>
                        </li>
                     </ul>
                     <h3>{{$newsDetail->title}}</h3>
                  </div>
                  <div class="blog-detail-image">
                     <img src="{{$newsDetail->image}}" alt="">
                  </div>
                  <div class="blog-detail-info">
                     <p>{!! $newsDetail->description !!}</p>
                    
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   
   <!-- <section class="similar-blog-section pt-0">
      <div class="container">
         <div class="row">
            <dv class="col-md-12">
               <div class="section-title mb-4">
                  <h2>Similar News</h2>
               </div>
            </dv>
         </div>
         <div class="row">
            <div class="col-md-4">
               <div class="blog-main">
                  <div class="blog-image">
                     <img src="assets/img/blog/1.jpg" class="w-100" alt="">
                  </div>
                  <div class="blog-content">
                     <h3>Basics First</h3>
                     <p>Lorem ipsum dolor sit amet, est ne zril tibique. Pro ei purto legere maluisset. Quod vidisse...
                     </p>
                     <ul>
                        <li>
                           <i class='bx bx-calendar'></i> <span> Oct 21, 2022</span>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="blog-main">
                  <div class="blog-image">
                     <img src="assets/img/blog/2.jpg" class="w-100" alt="">
                  </div>
                  <div class="blog-content">
                     <h3>Becoming Great</h3>
                     <p>Lorem ipsum dolor sit amet, est ne zril tibique. Pro ei purto legere maluisset. Quod vidisse...
                     </p>
                     <ul>
                        <li>
                           <i class='bx bx-calendar'></i> <span> Oct 21, 2022</span>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="blog-main">
                  <div class="blog-image">
                     <img src="assets/img/blog/3.jpg" class="w-100" alt="">
                  </div>
                  <div class="blog-content">
                     <h3>Better Results</h3>
                     <p>Lorem ipsum dolor sit amet, est ne zril tibique. Pro ei purto legere maluisset. Quod vidisse...
                     </p>
                     <ul>
                        <li>
                           <i class='bx bx-calendar'></i> <span> Oct 21, 2022</span>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section> -->
@endsection
