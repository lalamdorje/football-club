<?php

namespace Database\Seeders;

use App\Models\Team;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teams = [
            [
                'name' => 'Marc-André ter Stegen',
                'number' => 1,
                'player_role_id' => '4',
            ],
            [
                'name' => 'Iñaki Peña',
                'number' => 26,
                'player_role_id' => '4',
            ],
            [
                'name' => 'Gerard Piqué',
                'number' => 3,
                'player_role_id' => '1',
            ],
            [
                'name' => 'Marcos Alonso',
                'number' => 17,
                'player_role_id' => '1',
            ],
            [
                'name' => 'Jordi Alba',
                'number' => 18,
                'player_role_id' => '1',
            ],
            [
                'name' => 'Jules Kounde',
                'number' => 23,
                'player_role_id' => '1',
            ],
            [
                'name' => 'Andreas Christensen',
                'number' => 15,
                'player_role_id' => '1',
            ],
            [
                'name' => 'Ronald Araujo',
                'number' => 4,
                'player_role_id' => '1',
            ],
            [
                'name' => 'Frenkie De Jong',
                'number' => 21,
                'player_role_id' => '3',
            ],
            [
                'name' => 'Gavi',
                'number' => 30,
                'player_role_id' => '3',
            ],
            [
                'name' => 'Pedri',
                'number' => 8,
                'player_role_id' => '3',
            ],
            [
                'name' => 'Sergio Busquets',
                'number' => 5,
                'player_role_id' => '3',
            ],
            [
                'name' => 'Ansu Fati',
                'number' => 10,
                'player_role_id' => '2',
            ],
            [
                'name' => 'Ousmane Dembélé',
                'number' => 7,
                'player_role_id' => '2',
            ],
            [
                'name' => 'Robert Lewandowski',
                'number' => 9,
                'player_role_id' => '2',
            ],
            [
                'name' => 'Ferran Torres',
                'number' => 11,
                'player_role_id' => '2',
            ],
            [
                'name' => 'Memphis Depay',
                'number' => 14,
                'player_role_id' => '2',
            ],
            [
                'name' => 'Raphinha',
                'number' => 22,
                'player_role_id' => '2',
            ]
        ];
        foreach ($teams as $key => $team) {
            $t = Team::create($team);
            $t->addMediaFromUrl(asset('/frontend/teams/' . ($key + 1) . '.png'))->toMediaCollection();
        }
    }
}
