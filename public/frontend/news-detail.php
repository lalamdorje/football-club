<?php include('partials/header.php')  ?>

   <section class="breadcrumbs" style="background-image: url('assets/img/breadcrumb-bg.jpg');">
      <div class="container">
         <ol>
            <li><a href="index.html">Home</a></li>
            <li>News Detail</li>
         </ol>
         <h2>News Detail</h2>
      </div>
   </section>
   <section class="blog-detail-section">
      <div class="container">
         <div class="row">
            <div class="col-md-10 offset-md-1">
               <div class="blog-detail-main">
                  <div class="blog-detail-head text-center">
                     <ul>
                        <li>
                           <i class='bx bx-calendar'></i> <span> Oct 21, 2022</span>
                        </li>
                     </ul>
                     <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                  </div>
                  <div class="blog-detail-image">
                     <img src="assets/img/about/3.jpg" alt="">
                  </div>
                  <div class="blog-detail-info">
                     <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                     <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit
                        amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section class="similar-blog-section pt-0">
      <div class="container">
         <div class="row">
            <dv class="col-md-12">
               <div class="section-title mb-4">
                  <h2>Similar News</h2>
               </div>
            </dv>
         </div>
         <div class="row">
            <div class="col-md-4">
               <div class="blog-main">
                  <div class="blog-image">
                     <img src="assets/img/blog/1.jpg" class="w-100" alt="">
                  </div>
                  <div class="blog-content">
                     <h3>Basics First</h3>
                     <p>Lorem ipsum dolor sit amet, est ne zril tibique. Pro ei purto legere maluisset. Quod vidisse...
                     </p>
                     <ul>
                        <li>
                           <i class='bx bx-calendar'></i> <span> Oct 21, 2022</span>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="blog-main">
                  <div class="blog-image">
                     <img src="assets/img/blog/2.jpg" class="w-100" alt="">
                  </div>
                  <div class="blog-content">
                     <h3>Becoming Great</h3>
                     <p>Lorem ipsum dolor sit amet, est ne zril tibique. Pro ei purto legere maluisset. Quod vidisse...
                     </p>
                     <ul>
                        <li>
                           <i class='bx bx-calendar'></i> <span> Oct 21, 2022</span>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="blog-main">
                  <div class="blog-image">
                     <img src="assets/img/blog/3.jpg" class="w-100" alt="">
                  </div>
                  <div class="blog-content">
                     <h3>Better Results</h3>
                     <p>Lorem ipsum dolor sit amet, est ne zril tibique. Pro ei purto legere maluisset. Quod vidisse...
                     </p>
                     <ul>
                        <li>
                           <i class='bx bx-calendar'></i> <span> Oct 21, 2022</span>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <?php include('partials/footer.php')  ?>
