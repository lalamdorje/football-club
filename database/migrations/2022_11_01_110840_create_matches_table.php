<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('opponent_team_id');
            $table->foreign('opponent_team_id')->references('id')->on('opponent_teams')->onUpdate('cascade')->onDelete('cascade');
            $table->date('date');
            $table->string('result')->nullable();
            $table->integer('goal_scored')->nullable();
            $table->integer('goal_conceded')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
};
