@extends('backend.layouts.app')
@section('styles')
    <link rel="stylesheet" type="text/css" href="https://jeremyfagis.github.io/dropify/dist/css/dropify.min.css">
@endsection
@section('content')
    <!-- Image Slider Header -->
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">Image Slider Section</span>
            <h3 class="page-title">Add New Image Slider</h3>
        </div>
    </div>
    <!-- End Image Slider Header -->
    <form class="add-new-post" action="{{isset($imageSlider)?route('image_slider.update', $imageSlider):route('image_slider.store')}}" method="post" enctype="multipart/form-data">
        <div class="row">
            <div class="col-lg-9 col-md-12">
                <!-- Add New Post Form -->
                <div class="card card-small mb-3">
                    <div class="card-body">
                        @csrf
                        @if(isset($imageSlider))
                            @method('PATCH')
                        @endif
                        <div class="col-md-12 col-sm-12 p-0 pl-3">
                            <div class="row col-md-12 mb-5">
                                <div class="col-md-4">
                                    <label>Image Slider<span class="required">*</span></label>
                                </div>
                                <div class="col-md-7 card p-3">
                                    <input name="image" type="file" class="dropify" data-height="200"
                                           data-default-file="{{isset($imageSlider) ? $imageSlider->image : ''}}" accept="image/*"/>
                                    @if($errors->first('image'))
                                        <div class="text text-danger">
                                            * {{$errors->first('image')}}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 p-0 pl-3">
                            <label>Title <span class="required">*</span> </label>
                            <input class="form-control form-control-lg mb-3" id="title" type="text" name="title" value="{{old('title')?old('title'):(isset($imageSlider)?$imageSlider->title:'')}}" placeholder="Your Image Slider Title">
                            @if($errors->first('title'))
                                <div class="text text-danger">
                                    {{$errors->first('title')}}
                                </div>
                            @endif
                        </div>

                        <div class="col-md-12 col-sm-12 p-0 pl-3">
                            <label>Subtitle <span class="required">*</span> </label>
                            <input class="form-control form-control-lg mb-3" id="subtitle" type="text" name="subtitle" value="{{old('subtitle')?old('subtitle'):(isset($imageSlider)?$imageSlider->subtitle:'')}}" placeholder="Your Image Slider Subtitle">
                            @if($errors->first('subtitle'))
                                <div class="text text-danger">
                                    {{$errors->first('subtitle')}}
                                </div>
                            @endif
                        </div>

                        <div class="col-md-12 col-sm-12 p-0 pl-3">
                            <label>Description</label>
                            @if($errors->first('description'))
                                <div class="text text-danger">
                                    *{{$errors->first('description')}}
                                </div>
                            @endif
                            <textarea id="mytextarea"
                                      name="description">{!! isset($imageSlider)?$imageSlider->description:(old('description') ?? '') !!}
                        </textarea>
                        </div>

                    </div>
                </div>
                <!-- / Add New Post Form -->
            </div>
            <div class="col-lg-3 col-md-12">
                <!-- Post Overview -->
                <div class='card card-small mb-3'>
                    <div class="card-header border-bottom">
                        <h6 class="m-0">Actions</h6>
                    </div>
                    <div class='card-body p-0'>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item d-flex px-3">
                                    <button type="submit" id="draft" class="btn btn-sm btn-outline-accent" name="status" value="draft">
                                        <i class="material-icons">save</i> Save Draft
                                    </button>
                                    <button type="submit" class="btn btn-sm btn-accent ml-auto">
                                        <i class="material-icons">file_copy</i> Publish
                                    </button>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- / Post Overview -->
            </div>
        </div>
    </form>
@endsection
@section('js')
    <script type="text/javascript" src="https://jeremyfagis.github.io/dropify/dist/js/dropify.min.js"></script>
    <script>
        $('.dropify').dropify();
    </script>
@endsection
