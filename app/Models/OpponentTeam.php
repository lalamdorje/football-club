<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class OpponentTeam extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;
    protected $fillable = ['name', 'status'];
     protected $appends = ['logo'];

    public function getLogoAttribute()
    {
        return $this->hasMedia() ? $this->getMedia()[0]->getFullUrl() : 'Opponent Team';
    }
}
