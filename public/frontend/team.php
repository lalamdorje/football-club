<?php include('partials/header.php')  ?>

<section class="breadcrumbs" style="background-image: url('assets/img/breadcrumb-bg.jpg');">
    <div class="container">
        <ol>
            <li><a href="index.html">Home</a></li>
            <li>Our Team</li>
        </ol>
        <h2>Our Team</h2>
    </div>
</section>
<section class="team-inner-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mgmt-team">
                    <div class="section-title mb-2">
                        <h2>Goal Keepers</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="player-info">
                            <div class="player-image">
                                <img src="assets/img/player/4.jpg" class="w-100" alt="" />
                            </div>
                            <div class="player-desc">
                                <div class="player-number">
                                    <span>06</span>
                                </div>
                                <div class="player-name">
                                    <h3>Aron Ramsdale</h3>
                                    <p>Goal Keeper</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="player-info">
                            <div class="player-image">
                                <img src="assets/img/player/4.jpg" class="w-100" alt="" />
                            </div>
                            <div class="player-desc">
                                <div class="player-number">
                                    <span>06</span>
                                </div>
                                <div class="player-name">
                                    <h3>Aron Ramsdale</h3>
                                    <p>Goal keeper</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="player-info">
                            <div class="player-image">
                                <img src="assets/img/player/4.jpg" class="w-100" alt="" />
                            </div>
                            <div class="player-desc">
                                <div class="player-number">
                                    <span>06</span>
                                </div>
                                <div class="player-name">
                                    <h3>Aron Ramsdale</h3>
                                    <p>Goal keeper</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="section-title mb-2">
                <h2>Defenders</h2>
            </div>
            <div class="col-md-3">
                <div class="player-info">
                    <div class="player-image">
                        <img src="assets/img/player/4.jpg" class="w-100" alt="" />
                    </div>
                    <div class="player-desc">
                        <div class="player-number">
                            <span>06</span>
                        </div>
                        <div class="player-name">
                            <h3>Xavier Hernández</h3>
                            <p>Midfielder</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="player-info">
                    <div class="player-image">
                        <img src="assets/img/player/2.jpg" class="w-100" alt="" />
                    </div>
                    <div class="player-desc">
                        <div class="player-number">
                            <span>06</span>
                        </div>
                        <div class="player-name">
                            <h3>Xavier Hernández</h3>
                            <p>Midfielder</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="player-info">
                    <div class="player-image">
                        <img src="assets/img/player/3.jpg" class="w-100" alt="" />
                    </div>
                    <div class="player-desc">
                        <div class="player-number">
                            <span>06</span>
                        </div>
                        <div class="player-name">
                            <h3>Xavier Hernández</h3>
                            <p>Midfielder</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="player-info">
                    <div class="player-image">
                        <img src="assets/img/player/1.jpg" class="w-100" alt="" />
                    </div>
                    <div class="player-desc">
                        <div class="player-number">
                            <span>06</span>
                        </div>
                        <div class="player-name">
                            <h3>Xavier Hernández</h3>
                            <p>Midfielder</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="section-title mb-2">
                <h2>Midfielders</h2>
            </div>
            <div class="col-md-3">
                <div class="player-info">
                    <div class="player-image">
                        <img src="assets/img/player/4.jpg" class="w-100" alt="" />
                    </div>
                    <div class="player-desc">
                        <div class="player-number">
                            <span>06</span>
                        </div>
                        <div class="player-name">
                            <h3>Xavier Hernández</h3>
                            <p>Midfielder</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="player-info">
                    <div class="player-image">
                        <img src="assets/img/player/2.jpg" class="w-100" alt="" />
                    </div>
                    <div class="player-desc">
                        <div class="player-number">
                            <span>06</span>
                        </div>
                        <div class="player-name">
                            <h3>Xavier Hernández</h3>
                            <p>Midfielder</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="player-info">
                    <div class="player-image">
                        <img src="assets/img/player/3.jpg" class="w-100" alt="" />
                    </div>
                    <div class="player-desc">
                        <div class="player-number">
                            <span>06</span>
                        </div>
                        <div class="player-name">
                            <h3>Xavier Hernández</h3>
                            <p>Midfielder</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="player-info">
                    <div class="player-image">
                        <img src="assets/img/player/1.jpg" class="w-100" alt="" />
                    </div>
                    <div class="player-desc">
                        <div class="player-number">
                            <span>06</span>
                        </div>
                        <div class="player-name">
                            <h3>Xavier Hernández</h3>
                            <p>Midfielder</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="section-title mb-2">
                <h2>Strikers</h2>
            </div>
            <div class="col-md-3">
                <div class="player-info">
                    <div class="player-image">
                        <img src="assets/img/player/4.jpg" class="w-100" alt="" />
                    </div>
                    <div class="player-desc">
                        <div class="player-number">
                            <span>06</span>
                        </div>
                        <div class="player-name">
                            <h3>Xavier Hernández</h3>
                            <p>Midfielder</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="player-info">
                    <div class="player-image">
                        <img src="assets/img/player/2.jpg" class="w-100" alt="" />
                    </div>
                    <div class="player-desc">
                        <div class="player-number">
                            <span>06</span>
                        </div>
                        <div class="player-name">
                            <h3>Xavier Hernández</h3>
                            <p>Midfielder</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="player-info">
                    <div class="player-image">
                        <img src="assets/img/player/3.jpg" class="w-100" alt="" />
                    </div>
                    <div class="player-desc">
                        <div class="player-number">
                            <span>06</span>
                        </div>
                        <div class="player-name">
                            <h3>Xavier Hernández</h3>
                            <p>Midfielder</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="player-info">
                    <div class="player-image">
                        <img src="assets/img/player/1.jpg" class="w-100" alt="" />
                    </div>
                    <div class="player-desc">
                        <div class="player-number">
                            <span>06</span>
                        </div>
                        <div class="player-name">
                            <h3>Xavier Hernández</h3>
                            <p>Midfielder</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</section>
<?php include('partials/footer.php')  ?>
