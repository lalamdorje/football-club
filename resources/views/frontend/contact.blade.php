@extends('frontend.layouts.app')
@section('content')

<section class="breadcrumbs" style="background-image: url('{{asset('frontend/assets/img/breadcrumb-bg.jpg')}}');">
         <div class="container">
            <ol>
               <li><a href="{{route('frontend.index')}}">Home</a></li>
               <li>Contact Page</li>
            </ol>
            <h2>Contact Page</h2>
         </div>
      </section>
      <!-- End Breadcrumbs -->
      <section class="contact-section">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-md-5">
                  <div class="get-in-touch">
                     <h3>Get in touch</h3>
                     <h5>We’ love to hear from you. Our friendly team is always here to chat.</h5>
                     <div class="contact-info">
                        <div class="media">
                           <i class='bx bx-envelope' ></i>
                           <div class="media-body">
                              <h6 class="mt-0">Chat to us</h6>
                              <p>Our friendly team is here to help.</p>
                              <p>{{$siteSetting->email}}</p>
                           </div>
                        </div>
                     </div>
                     <div class="contact-info">
                        <div class="media">
                           <i class='bx bx-location-plus' ></i>
                           <div class="media-body">
                              <h6 class="mt-0">Office</h6>
                              <p>{{$siteSetting->address}}</p>
                           </div>
                        </div>
                     </div>
                     <div class="contact-info">
                        <div class="media">
                           <i class='bx bx-phone' ></i>
                           <div class="media-body">
                              <h6 class="mt-0">Phone</h6>
                              <p>Mon-Fri From 8AM-6PM</p>
                              <p>{{$siteSetting->phone}}</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-7">
                  <div class="contact-form">
                    @if(Session::has('msg') || Session::has('error'))
                        <div class="col-md-12 col-sm-5 float-right alert @if(Session::has('msg')) alert-success @else alert-danger @endif alert-dismissible m-2 mt-4 fade show" role="alert">
                            {{Session::get('msg') ?? Session::get('error')}}.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                     <h3>Contact Us</h3>
                     <p>You can reach us anytime via football@gmail.com</p>
                     <form action="{{route('frontend.contact.store')}}" method="POST" class="mt-4"  >
                        @csrf
                       <div class="mb-3">
                         <label for="name" class="form-label">Full Name</label>
                         <input type="text" class="form-control" id="name" name="name" placeholder="Your Name">
                       </div>
                       <div class="mb-3">
                         <label for="email" class="form-label">Email Address</label>
                         <input type="email" class="form-control" id="email"  name="email" placeholder="you@company.com">
                       </div>
                       <div class="mb-3">
                         <label for="phone" class="form-label">Phone Number</label>
                         <input type="number" class="form-control" id="phone"  name="phone" placeholder="+977-98xxxxxxxx">
                       </div>
                       <div class="mb-3">
                          <label for="message" class="form-label">How can we help?</label>
                          <textarea class="form-control" id="message" name="message" rows="4" placeholder="Tell us a about how can we help you.."></textarea>
                        </div>

                       <button type="submit" class="btn btn-primary">Submit</button>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="map-section pb-0 pt-0">
         <iframe src="{{$siteSetting->iframe}}" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
      </section>
@endsection
