<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta content="width=device-width, initial-scale=1.0" name="viewport">
      <title>Football Club</title>
      <meta content="" name="description">
      <meta content="" name="keywords">
      <!-- Favicons -->
      <link href="assets/img/favicon.png" rel="icon">
      <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
      <!-- Google Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
      <!-- Vendor CSS Files -->
      <link href="assets/vendor/aos/aos.css" rel="stylesheet">
      <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
      <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
      <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
      <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
      <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
      <link href="assets/vendor/fancybox/fancybox.min.css" rel="stylesheet">
      <link href="assets/vendor/owl-carousel/owl.carousel.min.css" rel="stylesheet">
      <link href="assets/vendor/owl-carousel/owl.theme.default.min.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
      <!-- Template Main CSS File -->
      <link href="assets/css/style.css" rel="stylesheet">
   </head>
   <body>
      <!-- ======= Header ======= -->
      <header id="header" class="fixed-top d-flex align-items-center">
         <div class="container d-flex align-items-center">
            <h1 class="logo me-auto"><a href="index.html">Football<span>.</span></a></h1>
            <nav id="navbar" class="navbar order-last order-lg-0">
               <ul>
                  <li><a class="nav-link active" href="#">Home</a></li>
                  <li><a class="nav-link" href="#">About</a></li>
                  <li><a class="nav-link" href="#">Services</a></li>
                  <li><a class="nav-link " href="#">Portfolio</a></li>
                  <li><a class="nav-link" href="#">Team</a></li>
                  <li><a href="blog.html">Blog</a></li>
                  <li class="dropdown">
                     <a href="#"><span>Drop Down</span> <i class="bi bi-chevron-down"></i></a>
                     <ul>
                        <li><a href="#">Drop Down 1</a></li>
                        <li class="dropdown">
                           <a href="#"><span>Deep Drop Down</span> <i class="bi bi-chevron-right"></i></a>
                           <ul>
                              <li><a href="#">Deep Drop Down 1</a></li>
                              <li><a href="#">Deep Drop Down 2</a></li>
                              <li><a href="#">Deep Drop Down 3</a></li>
                              <li><a href="#">Deep Drop Down 4</a></li>
                              <li><a href="#">Deep Drop Down 5</a></li>
                           </ul>
                        </li>
                        <li><a href="#">Drop Down 2</a></li>
                        <li><a href="#">Drop Down 3</a></li>
                        <li><a href="#">Drop Down 4</a></li>
                     </ul>
                  </li>
                  <li><a class="nav-link scrollto" href="#contact">Contact</a></li>
               </ul>
               <i class="bi bi-list mobile-nav-toggle"></i>
            </nav>
            <!-- .navbar -->
            <a href="#about" class="get-started-btn scrollto">Get Started</a>
         </div>
      </header>
      <section class="breadcrumbs" style="background-image: url('assets/img/breadcrumb-bg.jpg');">
         <div class="container">
            <ol>
               <li><a href="index.html">Home</a></li>
               <li>Product List Page</li>
            </ol>
            <h2>Product List Page</h2>
         </div>
      </section>
      <!-- End Breadcrumbs -->
      <section class="product-list-section">
         <div class="container">
            <div class="row">
               <div class="col-md-3">
                  <div class="product-info">
                     <div class="product-image">
                        <img src="assets/img/product/1.jpg" class="w-100" alt="">
                     </div>
                     <div class="product-desc">
                        <h6>Shoes</h6>
                        <h2>Men Football Shoes Boots Predator</h2>
                        <p>$125.25</p>
                     </div>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="product-info">
                     <div class="product-image">
                        <img src="assets/img/product/2.jpg" class="w-100" alt="">
                     </div>
                     <div class="product-desc">
                        <h6>Gloves</h6>
                        <h2>Alpha Goalkeeper Glove</h2>
                        <p>$125.25</p>
                     </div>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="product-info">
                     <div class="product-image">
                        <img src="assets/img/product/3.jpg" class="w-100" alt="">
                     </div>
                     <div class="product-desc">
                        <h6>Shoes</h6>
                        <h2>Athletic Training Boots</h2>
                        <p>$125.25</p>
                     </div>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="product-info">
                     <div class="product-image">
                        <img src="assets/img/product/4.jpg" class="w-100" alt="">
                     </div>
                     <div class="product-desc">
                        <h6>Gloves</h6>
                        <h2>Weather Grip Goalkeeper Gloves</h2>
                        <p>$125.25</p>
                     </div>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="product-info">
                     <div class="product-image">
                        <img src="assets/img/product/4.jpg" class="w-100" alt="">
                     </div>
                     <div class="product-desc">
                        <h6>Gloves</h6>
                        <h2>Weather Grip Goalkeeper Gloves</h2>
                        <p>$125.25</p>
                     </div>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="product-info">
                     <div class="product-image">
                        <img src="assets/img/product/3.jpg" class="w-100" alt="">
                     </div>
                     <div class="product-desc">
                        <h6>Shoes</h6>
                        <h2>Athletic Training Boots</h2>
                        <p>$125.25</p>
                     </div>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="product-info">
                     <div class="product-image">
                        <img src="assets/img/product/1.jpg" class="w-100" alt="">
                     </div>
                     <div class="product-desc">
                        <h6>Shoes</h6>
                        <h2>Men Football Shoes Boots Predator</h2>
                        <p>$125.25</p>
                     </div>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="product-info">
                     <div class="product-image">
                        <img src="assets/img/product/2.jpg" class="w-100" alt="">
                     </div>
                     <div class="product-desc">
                        <h6>Gloves</h6>
                        <h2>Alpha Goalkeeper Glove</h2>
                        <p>$125.25</p>
                     </div>
                  </div>
               </div>
               <div class="col-md-12">
                  <nav>
                     <ul class="pagination justify-content-end mt-4">
                        <li class="page-item disabled">
                           <a class="page-link" href="#" tabindex="-1">Previous</a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                           <a class="page-link" href="#">Next</a>
                        </li>
                     </ul>
                  </nav>
               </div>
            </div>
         </div>
      </section>
      <!-- ======= Footer ======= -->
      <footer id="footer">
         <div class="footer-top">
            <div class="container">
               <div class="row">
                  <div class="col-lg-3 col-md-6 footer-contact">
                     <h3>Football<span>.</span></h3>
                     <p>
                        Devkota Sadak<br>
                        New Baneshwor, Kathmandu<br>
                        Nepal <br><br>
                        <strong>Phone:</strong> +977-9841234567<br>
                        <strong>Email:</strong> info@football.com<br>
                     </p>
                  </div>
                  <div class="col-lg-5 col-md-6 footer-links">
                     <h4>Useful Links</h4>
                     <ul>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
                     </ul>
                  </div>
                  <div class="col-lg-4 col-md-6 footer-newsletter">
                     <h4>Join Our Newsletter</h4>
                     <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
                     <form action="" method="post">
                        <input type="email" name="email"><input type="submit" value="Subscribe">
                     </form>
                  </div>
               </div>
            </div>
         </div>
         <div class="container d-md-flex py-4">
            <div class="me-md-auto text-center text-md-start">
               <div class="copyright">
                  &copy; Copyright <strong><span>Football</span></strong>. All Rights Reserved
               </div>
               <div class="credits">
                  Designed by <a href="https://softmahal.com/" target="_blank" class="text-white">Softmahal Technologies</a>
               </div>
            </div>
            <div class="social-links text-center text-md-end pt-3 pt-md-0">
               <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
               <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
               <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
               <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
               <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
            </div>
         </div>
      </footer>
      <!-- End Footer -->
      <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
      <!-- Vendor JS Files -->
      <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js"></script> -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
      <script src="assets/vendor/aos/aos.js"></script>
      <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      <script src="assets/vendor/owl-carousel/owl.carousel.min.js"></script>
      <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
      <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
      <script type="text/javascript" src="assets/vendor/fancybox/fancybox.min.js"></script>
      <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
      <!-- Template Main JS File -->
      <script src="assets/js/main.js"></script>
      
   </body>
</html>