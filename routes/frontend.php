<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@index')->name('frontend.index');
Route::get('/team', 'HomeController@team')->name('frontend.team');
Route::get('/match', 'MatchController@index')->name('frontend.match');
Route::get('/news', 'NewsController@news')->name('frontend.news');
Route::get('/news/{id}/detail', 'NewsController@detail')->name('frontend.news_detail');
Route::get('about', 'HomeController@about')->name('frontend.about');
Route::get('contact', 'ContactController@contact')->name('frontend.contact');
Route::post('contact/store', 'ContactController@store')->name('frontend.contact.store');
