@extends('frontend.layouts.app')
@section('content')
    <section class="breadcrumbs" style="background-image: url('{{asset('frontend/assets/img/breadcrumb-bg.jpg')}}');">
        <div class="container">
            <ol>
                <li><a href="{{route('frontend.index')}}">Home</a></li>
                <li>News & Updates</li>
            </ol>
            <h2>News & Updates</h2>
        </div>
    </section>
    <!-- End Breadcrumbs -->
    <section class="news">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        @forelse($newses as $news)
                            <div class="col-md-12">
                                <div class="news-list">
                                    <a href="{{route('frontend.news_detail', $news->id)}}">
                                        <img src="{{ $news->image }}" class="w-100" alt=""/>
                                    </a>
                                    <h6><i class="bx bx-calendar"></i> {{ $news->created_at->format('d F, Y') }}</h6>
                                    <a href="{{route('frontend.news_detail', $news->id)}}">
                                        <h2>{{$news->title}}</h2>
                                    </a>
                                    <p>{!! $news->description !!} </p>
                                </div>
                            </div>
                        @empty
                        @endforelse
                    </div>
                </div>
                <div class="col-md-4 news-side">
                    <div class="section-title">
                        <h2>Latest News</h2>
                    </div>
                    @forelse($latestNews as $latestNew)
                        <div class="news-sub media news-list mb-4">
                            <a href="{{route('frontend.news_detail', $latestNew->id)}}">
                                <img src="{{ $latestNew->getMedia()[0]->getFullUrl() }}" class="mr-3" alt=""/>
                            </a>
                            <div class="media-body">
                                <a href="{{route('frontend.news_detail', $latestNew->id)}}">
                                    <h2 class="mt-0">{{$latestNew->title}}</h2>
                                </a>
                                <h6><i class="bx bx-calendar"></i> {{$latestNew->created_at->format('d F, Y')}}</h6>
                            </div>
                        </div>
                    @empty
                    @endforelse
                </div>
            </div>
        </div>
    </section>

@endsection
