@extends('backend.layouts.app')
@section('content')
    <?php

    use Carbon\Carbon; ?>
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <!-- <span class="text-uppercase page-subtitle">Overview</span> -->
            <h3 class="page-title">Match Fixture Table</h3>
            <a href="{{route('fixture_table.create')}}" class="btn btn-primary mt-4 ml-2"><i class="fa fa-plus"></i> Add New Team</a>
        </div>
    </div>
    <!-- End Page Header -->
    <!-- Default Light Table -->
    <div class="row">
        <div class="col">
            <div class="card card-small mb-4">
                <div class="card-body p-3 text-center">
                    <div class="table-responsive p-1">
                        <table id="event-table" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                            <tr>
                                <th>S.N</th>
                                <th>Name</th>
                                <th>Matches Played</th>
                                <th>Wins</th>
                                <th>Draws</th>
                                <th>Losses</th>
                                <th>Points</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $i = 1;
                            @endphp
                            @foreach($fixtureTables as $fixtureTable)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $fixtureTable->opponent_team->name ?? $siteSetting->name}}</td>
                                    <td>{{$fixtureTable['matches_played'] ?? 0}}</td>
                                    <td>{{$fixtureTable['wins'] ?? 0}}</td>
                                    <td>{{$fixtureTable['draws'] ?? 0}}</td>
                                    <td>{{$fixtureTable['losses'] ?? 0}}</td>
                                    <td>{{$fixtureTable['points'] ?? 0}}</td>
                                    <td>
                                        @isset($fixtureTable->opponent_team->name)
                                        <div class="d-flex justify-content-center">
                                            <a class="btn btn-warning btn-sm mr-2" href="{{route('fixture_table.edit',$fixtureTable)}}">Edit</a>
                                            <form action="{{route('fixture_table.destroy',$fixtureTable)}}" method="post" onsubmit="return confirm('Are you sure?')">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-sm btn-danger mr-2"><i class="fa fa-trash text-white" title="Delete"></i> </button>
                                            </form>
                                        </div>
                                        @endisset
                                    </td>
                                </tr>
                                <div class="modal fade" id="MatchScoreMenu" tabindex="-1" role="dialog" aria-labelledby="MatchScoreMenuTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-6">
                                                        <label>Goal Scored<span class="required">*</span> </label>
                                                        <input type="number" class="form-control form-control" id="goal_scored" name="goal_scored" value="{{old('goal_scored')?old('goal_scored') : ''}}" placeholder="Goal Scored">
                                                        <span id="error_goal_scored"></span>
                                                    </div>
                                                    <div class="col-6">
                                                        <label>Goal Conceded<span class="required">*</span> </label>
                                                        <input type="number" class="form-control form-control" id="goal_conceded" name="goal_conceded" value="{{old('goal_conceded')?old('goal_conceded') : ''}}" placeholder="Goal Scored">
                                                        <span id="error_goal_conceded"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary" id="storeScore">Save changes</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Default Light Table -->
    <script type="text/javascript">
        $(document).ready(function() {
            setTimeout(function() {
                $('.alert .close').click()
            }, 3000);
            $('#event-table').DataTable({
                "columnDefs": [{
                    "width": "100px",
                    "targets": 0
                },
                    {
                        "width": "150px",
                        "targets": 1
                    },
                    {
                        "width": "150px",
                        "targets": 2
                    },
                    {
                        "width": "70px",
                        "targets": 3
                    },
                ],
            });
        });
    </script>
@endsection
@section('js')
    <script>
        $(document).ready(function () {
            $('#MatchScoreMenu').on('show.bs.modal', function (e){
                var id = $(e.relatedTarget).data('id');
                $('#storeScore').data('id', id);
            })

            $('#storeScore').click(function (){
                const id = $(this).data('id');
                let goal_scored = $('#goal_scored').val();
                let goal_conceded = $('#goal_conceded').val();

                $.ajax({
                    type: 'GET',
                    url: '/admin/store-goals/' + id,
                    data: {goal_scored: goal_scored, goal_conceded:goal_conceded},
                    dataType: "json",
                    success: function (response) {
                        console.log(response)
                        // $('#finished').append(`<span  class="shadow-none badge badge-success">Finished</span>`)
                        window.location.reload();
                        // var response = JSON.parse(response);
                    }
                });
            });
        });
    </script>
@endsection
