<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class SiteSetting extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $fillable = ['name', 'description', 'email', 'address', 'phone', 'iframe', 'facebook_link', 'linkedin_link', 'instagram_link', 'twitter_link'];
    protected $appends = ['logo'];

    public function getLogoAttribute(){
        return $this->hasMedia() ? $this->getMedia()[0]->getFullUrl() : asset ('backend/images/logo.png');
    }
}
