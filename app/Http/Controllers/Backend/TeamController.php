<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\PlayerRole;
use Illuminate\Http\Request;
use App\Models\Team;
use App\Http\Requests\Backend\TeamRequest;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams = Team::latest()->get();
        return view('backend.team.index', compact('teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = PlayerRole::all();
        return view ('backend.team.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeamRequest $request)
    {
        $teams = Team::create($request->all());
        if ($request->hasFile('image')){
            $teams->addMedia($request->file('image'))->toMediaCollection();
        }
        return redirect()->route('team.index')->withMsg('Your Team has been successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Team $team)
    {
        $roles = PlayerRole::all();
        return view('backend.team.create', compact('team', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TeamRequest $request, Team $team)
    {
        if ($request->hasFile('image')){
            $team->clearMediaCollection();
            $team->addMedia($request->file('image'))->toMediaCollection();
        }
        $team->update($request->all());
        return redirect()->route('team.index')->withMsg('Your team has been successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Team $team)
    {
        if ($team->hasMedia()){
            $team->clearMediaCollection();
        }
        $team->delete();
        return redirect()->route('team.index')->withMsg('Your team has been successfully deleted.');
    }
}
