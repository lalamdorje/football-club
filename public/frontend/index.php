<?php include('partials/header.php')  ?>

      <!-- End Header -->
      <section class="banner-slider-section pt-0 pb-0">
         <div class="banner-slider owl-carousel owl-theme">
            <div class="item">
               <img src="assets/img/stadium-bg.jpg" alt="images not found">
               <div class="cover">
                  <div class="container">
                     <div class="row">
                        <div class="col-md-8">
                           <div class="header-content">
                              <div class="line"></div>
                              <h2>Welcome To Football</h2>
                              <h1>Sporting Club</h1>
                              <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                 tempor incididunt ut labore et dolore magna aliqua.
                              </h4>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="item">
               <img src="assets/img/stadium-bg.jpg" alt="images not found">
               <div class="cover">
                  <div class="container">
                     <div class="header-content">
                        <div class="line animated bounceInLeft"></div>
                        <h2>Reimagine Digital Experience with</h2>
                        <h1>Intelligent solutions</h1>
                        <h4>We help entrepreneurs, start-ups and enterprises shape their ideas into products</h4>
                     </div>
                  </div>
               </div>
            </div>
            <div class="item">
               <img src="assets/img/stadium-bg.jpg" alt="images not found">
               <div class="cover">
                  <div class="container">
                     <div class="header-content">
                        <div class="line animated bounceInLeft"></div>
                        <h2>Peimagine Digital Experience with</h2>
                        <h1>Intelligent Solutions</h1>
                        <h4>We help entrepreneurs, start-ups and enterprises shape their ideas into products</h4>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="last-match-section">
         <div class="container cont">
            <div class="row">
               <div class="col-md-3">
                  <div class="last-match">
                     <div class="club-logo">
                        <img src="assets/img/logo/1.png" alt="">
                        <h4>Manchester City</h4>
                     </div>
                     <div class="club-result">
                        <p>July 23 , 2022</p>
                        <h3>10 - 3</h3>
                        <p>Etihad Stadium</p>
                     </div>
                     <div class="club-logo">
                        <img src="assets/img/logo/2.png" alt="">
                        <h4>Manchester United</h4>
                     </div>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="last-match">
                     <div class="club-logo">
                        <img src="assets/img/logo/1.png" alt="">
                        <h4>Manchester City</h4>
                     </div>
                     <div class="club-result">
                        <p>July 23 , 2022</p>
                        <h3>10 - 3</h3>
                        <p>Etihad Stadium</p>
                     </div>
                     <div class="club-logo">
                        <img src="assets/img/logo/2.png" alt="">
                        <h4>Manchester United</h4>
                     </div>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="last-match">
                     <div class="club-logo">
                        <img src="assets/img/logo/1.png" alt="">
                        <h4>Manchester City</h4>
                     </div>
                     <div class="club-result">
                        <p>July 23 , 2022</p>
                        <h3>10 - 3</h3>
                        <p>Etihad Stadium</p>
                     </div>
                     <div class="club-logo">
                        <img src="assets/img/logo/2.png" alt="">
                        <h4>Manchester United</h4>
                     </div>
                  </div>
               </div>
               <div class="col-md-3 align-self-center">
                  <div class="last-match-head">
                     <h4>Results</h4>
                     <h2>The Last Results</h2>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="about-section">
         <div class="container">
            <div class="row">
               <div class="col-md-5">
                  <div class="section-title">
                     <h2>Our Story</h2>
                  </div>
                  <div class="about-main">
                     <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                     </p>
                     <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                     </p>
                     <a href="#">Read More</a>
                  </div>
               </div>
               <div class="col-md-7">
                  <div class="about-image">
                     <img src="assets/img/about.png" class="w-100" alt="">
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="games-section" style="background-image: url('assets/img/football-bg.jpg');">
         <div class="container">
            <div class="row">
               <div class="col-md-8 offset-md-2">
                  <div class="recent-match">
                     <div class="recent-match-head">
                        <h3>Upcoming Matches</h3>
                     </div>
                     <div class="recent-slider owl-carousel owl-theme">
                        <div class="item">
                           <div class="row">
                              <div class="col-md-4">
                                 <div class="logo-team">
                                    <img src="assets/img/logo/1.png" alt="">
                                    <h3>Manchester City</h3>
                                 </div>
                              </div>
                              <div class="col-md-4 align-self-center">
                                 <div class="final-score">
                                    <div class="score">VS</div>
                                    <div class="info"><i class='bx bx-calendar'></i> April 04, 2019 - 08:30 PM</div>
                                    <div class="info"><i class='bx bx-map-alt'></i> Wanda Metropolitano Stadium</div>
                                 </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="logo-team">
                                    <img src="assets/img/logo/2.png" alt="">
                                    <h3>Manchester United</h3>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="item">
                           <div class="row">
                              <div class="col-md-4">
                                 <div class="logo-team">
                                    <img src="assets/img/logo/3.png" alt="">
                                    <h3>Manchester City</h3>
                                 </div>
                              </div>
                              <div class="col-md-4 align-self-center">
                                 <div class="final-score">
                                    <div class="score">VS</div>
                                    <div class="info"><i class='bx bx-calendar'></i> April 04, 2019 - 08:30 PM</div>
                                    <div class="info"><i class='bx bx-map-alt'></i> Wanda Metropolitano Stadium</div>
                                 </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="logo-team">
                                    <img src="assets/img/logo/4.png" alt="">
                                    <h3>Manchester United</h3>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="team-player-section">
         <div class="container">
            <div class="row">
               <dv class="col-md-12">
                  <div class="section-title text-center mb-4">
                     <h2>Team Player</h2>
                  </div>
               </dv>
            </div>
            <div class="row">
               <div class="player-slider owl-carousel owl-theme">
                  <div class="item">
                     <div class="player-info">
                        <div class="player-image">
                           <img src="assets/img/player/2.jpg" class="w-100" alt="">
                        </div>
                        <div class="player-desc">
                           <div class="player-number">
                              <span>10</span>
                           </div>
                           <div class="player-name">
                              <h3>Lionel Andrés Messi</h3>
                              <p>Forward</p>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="item">
                     <div class="player-info">
                        <div class="player-image">
                           <img src="assets/img/player/3.jpg" class="w-100" alt="">
                        </div>
                        <div class="player-desc">
                           <div class="player-number">
                              <span>05</span>
                           </div>
                           <div class="player-name">
                              <h3>Carles Puyol</h3>
                              <p>Defender</p>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="item">
                     <div class="player-info">
                        <div class="player-image">
                           <img src="assets/img/player/4.jpg" class="w-100" alt="">
                        </div>
                        <div class="player-desc">
                           <div class="player-number">
                              <span>06</span>
                           </div>
                           <div class="player-name">
                              <h3>Xavier Hernández</h3>
                              <p>Midfielder</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="news-section pt-0">
         <div class="container">
         <div class="row">
            <div class="col-md-8">
               <div class="latest-news">
                  <div class="section-title">
                     <h2>Latest News</h2>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-6">
                     <div class="news-list">
                        <img src="assets/img/news/1.jpg" class="w-100" alt="">
                        <h6><i class='bx bx-calendar' ></i> 22 July 2022</h6>
                        <h2>Latest Point Table For The Premier League</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                           tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
                        </p>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="news-list">
                        <img src="assets/img/news/2.jpg" class="w-100" alt="">
                        <h6><i class='bx bx-calendar' ></i> 22 July 2022</h6>
                        <h2>Latest Point Table For The Premier League</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                           tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
                        </p>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="news-list">
                        <img src="assets/img/news/3.jpg" class="w-100" alt="">
                        <h6><i class='bx bx-calendar' ></i> 22 July 2022</h6>
                        <h2>Latest Point Table For The Premier League</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                           tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
                        </p>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="news-list">
                        <img src="assets/img/news/4.jpg" class="w-100" alt="">
                        <h6><i class='bx bx-calendar' ></i> 22 July 2022</h6>
                        <h2>Latest Point Table For The Premier League</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                           tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam
                        </p>
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="view-all">
                        <a href="#">View All News</a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="section-title">
                  <h2>League Table</h2>
               </div>
               <div class="league-table">
                  <table class="table">
                     <thead>
                        <tr >
                           <th>SN</th>
                           <th>Team</th>
                           <th>W</th>
                           <th>L</th>
                           <th>PTS</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <td>1</td>
                           <td>Barcelona</td>
                           <td>26</td>
                           <td>3</td>
                           <td>83</td>
                        </tr>
                        <tr>
                           <td>2</td>
                           <td>Arsenal</td>
                           <td>24</td>
                           <td>4</td>
                           <td>80</td>
                        </tr>
                        <tr>
                           <td>3</td>
                           <td>Man City</td>
                           <td>24</td>
                           <td>3</td>
                           <td>79</td>
                        </tr>
                        <tr>
                           <td>4</td>
                           <td>Manchester Unt</td>
                           <td>22</td>
                           <td>3</td>
                           <td>75</td>
                        </tr>
                        <tr>
                           <td>5</td>
                           <td>Liverpool</td>
                           <td>22</td>
                           <td>4</td>
                           <td>74</td>
                        </tr>
                        <tr>
                           <td>6</td>
                           <td>Chelsea</td>
                           <td>20</td>
                           <td>4</td>
                           <td>70</td>
                        </tr>
                        <tr>
                           <td>7</td>
                           <td>Atletico Madrid</td>
                           <td>18</td>
                           <td>6</td>
                           <td>64</td>
                        </tr>
                        <tr>
                           <td>8</td>
                           <td>Valencia</td>
                           <td>15</td>
                           <td>9</td>
                           <td>55</td>
                        </tr>
                        <tr>
                           <td>9</td>
                           <td>Real Sociedad</td>
                           <td>12</td>
                           <td>10</td>
                           <td>48</td>
                        </tr>
                        <tr>
                           <td>10</td>
                           <td>Real Madrid</td>
                           <td>12</td>
                           <td>12</td>
                           <td>48</td>
                        </tr>
                     </tbody>
                  </table>
               </div>
               <div class="section-title pt-3">
                  <h2>Our Sponsers</h2>
               </div>
               <div class="sponser">
                  <img src="assets/img/sponser.jpg" class="w-100" alt="">
               </div>
            </div>
         </div>
      </section>
      <!-- <section class="video-section" style="background-image: url('assets/img/video-bg.jpg');">
        <div class="container" style="margin-top: -13%;">
          <div style="--swiper-navigation-color: #fff; --swiper-pagination-color: #fff;" class="swiper mySwiper2">
            <div class="swiper-wrapper">
              <div class="swiper-slide">
                <div class="video-content">
                  <div class="video-thumbnail">
                    <img src="assets/img/video/1.jpg" class="w-100" alt="">
                    <div class="pulses">
                      <button type="button" class="play-btn js-video-button">
                      <a data-fancybox href="https://www.youtube.com/embed/zpOULjyy-n8?rel=0"
                        allowfullscreen></iframe>
                        <i class='bx bx-play'></i>
                      </a>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <div class="swiper-slide">
                <div class="video-content">
                  <div class="video-thumbnail">
                    <img src="assets/img/video/2.jpg" class="w-100" alt="">
                    <div class="pulses">
                      <button type="button" class="play-btn js-video-button">
                      <a data-fancybox href="https://www.youtube.com/embed/zpOULjyy-n8?rel=0"
                        allowfullscreen></iframe>
                        <i class='bx bx-play'></i>
                      </a>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <div class="swiper-slide">
                <div class="video-content">
                  <div class="video-thumbnail">
                    <img src="assets/img/video/3.jpg" class="w-100" alt="">
                    <div class="pulses">
                      <button type="button" class="play-btn js-video-button">
                      <a data-fancybox href="https://www.youtube.com/embed/zpOULjyy-n8?rel=0"
                        allowfullscreen></iframe>
                        <i class='bx bx-play'></i>
                      </a>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <div class="swiper-slide">
                <div class="video-content">
                  <div class="video-thumbnail">
                    <img src="assets/img/video/4.jpg" class="w-100" alt="">
                    <div class="pulses">
                      <button type="button" class="play-btn js-video-button">
                      <a data-fancybox href="https://www.youtube.com/embed/zpOULjyy-n8?rel=0"
                        allowfullscreen></iframe>
                        <i class='bx bx-play'></i>
                      </a>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <div class="swiper-slide">
                <div class="video-content">
                  <div class="video-thumbnail">
                    <img src="assets/img/video/5.jpg" class="w-100" alt="">
                    <div class="pulses">
                      <button type="button" class="play-btn js-video-button">
                      <a data-fancybox href="https://www.youtube.com/embed/zpOULjyy-n8?rel=0"
                        allowfullscreen></iframe>
                        <i class='bx bx-play'></i>
                      </a>
                      </button>
                    </div>
                  </div>
                </div>
              </div>              
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
          </div>
          <div class="match-head">
            <h1>Recent Match Highlights</h1>
          </div>
          <div thumbsSlider="" class="swiper mySwiper thumbnail">
            <div class="swiper-wrapper">
              <div class="swiper-slide">
                <img src="assets/img/video/1.jpg" />
              </div>
              <div class="swiper-slide">
                <img src="assets/img/video/2.jpg" />
              </div>
              <div class="swiper-slide">
                <img src="assets/img/video/3.jpg" />
              </div>
              <div class="swiper-slide">
                <img src="assets/img/video/4.jpg" />
              </div>
              <div class="swiper-slide">
                <img src="assets/img/video/5.jpg" />
              </div>              
            </div>
          </div>
        </div>
      </section> -->
      <section class="product-section">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="mb-3 position-relative text-end" >
                          <a class="text-white" href="#">View All Product</a>
                       </div>
                </div>
              <div class="col-md-3">
                <div class="product-info">
                        <div class="product-image">
                           <img src="assets/img/pro3.jpg" class="w-100" alt="">
                        </div>
                        <div class="product-desc">
                          <h2>Men Football Shoes Boots Predator</h2>
                          <p>$125.25</p>
                        </div>
                     </div>
              </div>
              <div class="col-md-3">
                <div class="product-info">
                        <div class="product-image">
                           <img src="assets/img/pro1.jpg" class="w-100" alt="">
                        </div>
                        <div class="product-desc">
                          <h2>Alpha Goalkeeper Glove</h2>
                          <p>$125.25</p>
                        </div>
                     </div>
              </div>
              <div class="col-md-3">
                <div class="product-info">
                        <div class="product-image">
                           <img src="assets/img/pro2.jpg" class="w-100" alt="">
                        </div>
                        <div class="product-desc">
                          <h2>Athletic Training Boots</h2>
                          <p>$125.25</p>
                        </div>
                     </div>
              </div>
              <div class="col-md-3">
                <div class="product-info">
                        <div class="product-image">
                           <img src="assets/img/pro1.jpg" class="w-100" alt="">
                        </div>
                        <div class="product-desc">
                          <h2>Weather Grip Goalkeeper Gloves</h2>
                          <p>$125.25</p>
                        </div>
                     </div>
              </div>
            </div>
         </div>
      </section>
      <section class="gallery-section pb-0 pt-0">
        <div class="row no-gutters">
          <div class="col-md-2">
            <div class="gallery-image">
              <a href="assets/img/gallery/1.jpg" data-fancybox="gallery">
                <img src="assets/img/gallery/1.jpg" alt="" />
              </a>              
            </div>
          </div>
          <div class="col-md-2">
            <div class="gallery-image">
             <a href="assets/img/gallery/2.jpg" data-fancybox="gallery">
                <img src="assets/img/gallery/2.jpg" alt="" />
              </a>   
            </div>
          </div>
          <div class="col-md-2">
            <div class="gallery-image">
              <a href="assets/img/gallery/3.jpg" data-fancybox="gallery">
                <img src="assets/img/gallery/3.jpg" alt="" />
              </a>   
            </div>
          </div>
          <div class="col-md-2">
            <div class="gallery-image">
              <a href="assets/img/gallery/4.jpg" data-fancybox="gallery">
                <img src="assets/img/gallery/4.jpg" alt="" />
              </a>   
            </div>
          </div>
          <div class="col-md-2">
            <div class="gallery-image">
              <a href="assets/img/gallery/5.jpg" data-fancybox="gallery">
                <img src="assets/img/gallery/5.jpg" alt="" />
              </a>   
            </div>
          </div>
          <div class="col-md-2">
            <div class="gallery-image">
              <a href="assets/img/gallery/1.jpg" data-fancybox="gallery">
                <img src="assets/img/gallery/1.jpg" alt="" />
              </a>   
            </div>
          </div>
        </div>
      </section>
      <!-- End #main -->
      <?php include('partials/footer.php')  ?>

      