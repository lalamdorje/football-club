@extends('backend.layouts.app')
@section('content')
    <!-- Player Header -->
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">Match Fixture Section</span>
            <h3 class="page-title">Add New Match</h3>
        </div>
    </div>
    <!-- End Player Header -->
    <form class="add-new-post"
          action="{{isset($fixtureTable)?route('fixture_table.update', $fixtureTable):route('fixture_table.store')}}"
          method="post" enctype="multipart/form-data">
        <div class="row">
            <div class="col-lg-9 col-md-12">
                <!-- Add New Post Form -->
                <div class="card card-small mb-3">
                    <div class="card-body">
                        @csrf
                        @if(isset($fixtureTable))
                            @method('PATCH')
                        @endif
                        <div class="col-md-12 col-sm-12 p-0 pl-3">
                            <label>Name<span class="required">*</span> </label>
                            <select class="form-control form-control-lg mb-3" id="opponent_team_id"
                                    name="opponent_team_id">
                                <option value="">Select Opponent Team</option>
                                @foreach($opponentTeams as $opponentTeam)
                                    <option
                                        value="{{$opponentTeam->id}}" {{(isset($fixtureTable) && $fixtureTable->opponent_team_id == $opponentTeam->id) ? 'selected':''}}>{{$opponentTeam->name}}</option>
                                @endforeach
                            </select>
                            @if($errors->first('opponent_team_id'))
                                <div class="text text-danger">
                                    {{$errors->first('opponent_team_id')}}
                                </div>
                            @endif
                        </div>

                        <div class="col-md-12 col-sm-12 p-0 pl-3">
                            <label>Wins<span class="required">*</span> </label>
                            <input class="form-control form-control-lg mb-3" id="wins" type="number" name="wins"
                                   value="{{old('wins')?old('wins'):(isset($fixtureTable)?$fixtureTable->wins:'')}}"
                                   placeholder="Wins">
                            @if($errors->first('wins'))
                                <div class="text text-danger">
                                    {{$errors->first('wins')}}
                                </div>
                            @endif
                        </div>
                        <div class="col-md-12 col-sm-12 p-0 pl-3">
                            <label>Draws<span class="required">*</span> </label>
                            <input class="form-control form-control-lg mb-3" id="draws" type="number" name="draws"
                                   value="{{old('draws')?old('draws'):(isset($fixtureTable)?$fixtureTable->draws:'')}}"
                                   placeholder="Draws">
                            @if($errors->first('draws'))
                                <div class="text text-danger">
                                    {{$errors->first('draws')}}
                                </div>
                            @endif
                        </div>
                        <div class="col-md-12 col-sm-12 p-0 pl-3">
                            <label>Losses<span class="required">*</span> </label>
                            <input class="form-control form-control-lg mb-3" id="losses" type="number" name="losses"
                                   value="{{old('losses')?old('losses'):(isset($fixtureTable)?$fixtureTable->losses:'')}}"
                                   placeholder="Losses">
                            @if($errors->first('losses'))
                                <div class="text text-danger">
                                    {{$errors->first('losses')}}
                                </div>
                            @endif
                        </div>
                        <div class="col-md-12 col-sm-12 p-0 pl-3">
                            <label>Points<span class="required">*</span> </label>
                            <input class="form-control form-control-lg mb-3" id="points" type="number" name="points"
                                   value="{{old('points')?old('points'):(isset($fixtureTable)?$fixtureTable->points:'')}}"
                                   placeholder="Points">
                            @if($errors->first('points'))
                                <div class="text text-danger">
                                    {{$errors->first('points')}}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- / Add New Post Form -->
            </div>
            <div class="col-lg-3 col-md-12">
                <!-- Post Overview -->
                <div class='card card-small mb-3'>
                    <div class="card-header border-bottom">
                        <h6 class="m-0">Actions</h6>
                    </div>
                    <div class='card-body p-0'>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item d-flex px-3">
                                <button type="submit" id="draft" class="btn btn-sm btn-outline-accent" name="status"
                                        value="draft">
                                    <i class="material-icons">save</i> Save Draft
                                </button>
                                <button type="submit" class="btn btn-sm btn-accent ml-auto">
                                    <i class="material-icons">file_copy</i> Publish
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- / Post Overview -->
            </div>
        </div>
    </form>
@endsection
@section('js')
    <script>
        $(document).ready(function () {
            let points = 0;
            $('input[type=number]').keyup(function () {
                let wins = $('#wins').val();
                let draws = $('#draws').val();
                points = wins * 3 + draws * 1;
                $('#points').val(points);
            });
        });
    </script>
@endsection
