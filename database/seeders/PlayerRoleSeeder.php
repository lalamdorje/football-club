<?php

namespace Database\Seeders;

use App\Models\PlayerRole;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PlayerRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name' => 'Defender',
                'order' => 2,
            ],
            [
                'name' => 'Striker',
                'order' => 4,
            ],
            [
                'name' => 'Mid-Fielder',
                'order' => 3,
            ],
            [
                'name' => 'Goal-Keeper',
                'order' => 1,
            ],
        ];
        foreach ($roles as $role) {
            PlayerRole::create($role);
        }
    }
}
