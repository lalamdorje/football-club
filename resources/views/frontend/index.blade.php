@extends('frontend.layouts.app')
@section('content')
    <section class="banner-slider-section pt-0 pb-0">
        <div class="banner-slider owl-carousel owl-theme">
            @foreach($imageSliders as $imageSlider)
                <div class="item">
                    <img src="{{$imageSlider->image}}" alt="images not found">
                    <div class="cover">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="header-content">
                                        <div class="line"></div>
                                        <h2>{{$imageSlider->subtitle}}</h2>
                                        <h1>{{$imageSlider->title}}</h1>
                                        <h4>
                                            {!! $imageSlider->description !!}
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
    <section class="last-match-section">
        <div class="container cont">
            <div class="row">
                @foreach($completedMatches as $completedMatch)
                    <div class="col-md-3">
                        <div class="last-match">
                            <div class="club-logo">
                                <img src="{{$siteSetting->logo}}" alt="">
                                <h4>{{$siteSetting->name}}</h4>
                            </div>
                            <div class="club-result">
                                <p> {{date('d F, Y', strtotime($completedMatch->date))}}</p>
                                <h3>{{$completedMatch->goal_scored}} - {{$completedMatch->goal_conceded}}</h3>
                            </div>
                            <div class="club-logo">
                                <img src="{{$completedMatch->opponent_team->logo}}" alt="">
                                <h4>{{$completedMatch->opponent_team->name}}</h4>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="col-md-3 align-self-center">
                    <div class="last-match-head">
                        <h4>Results</h4>
                        <h2>The Last Results</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="about-section">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="section-title">
                        <h2>{{$about->title}}</h2>
                    </div>
                    <div class="about-main">
                        <p>
                            {!! $about->description !!}
                        </p>
{{--                        <a href="#">Read More</a>--}}
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="about-image">
                        <img src="{{$about->image}}" class="w-100" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="games-section" style="background-image: url('frontend/assets/img/football-bg.jpg');">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <div class="recent-match">
                        <div class="recent-match-head">
                            <h3>Upcoming Matches</h3>
                        </div>
                        <div class="recent-slider owl-carousel owl-theme">
                            @foreach($matches as $match)
                                <div class="item">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="logo-team">
                                                <img src="{{$siteSetting->logo}}" alt="">
                                                <h3>{{$siteSetting->name}}</h3>
                                            </div>
                                        </div>
                                        <div class="col-md-4 align-self-center">
                                            <div class="final-score">
                                                <div class="score">VS</div>
                                                <div class="info"><i class='bx bx-calendar'></i> {{date('d F, Y', strtotime($match->date))}}</div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="logo-team">
                                                <img src="{{$match->opponent_team->logo}}" alt="">
                                                <h3>{{$match->opponent_team->name}}</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="team-player-section">
        <div class="container">
            <div class="row">
                <dv class="col-md-12">
                    <div class="section-title text-center mb-4">
                        <h2>Team Player</h2>
                    </div>
                </dv>
            </div>
            <div class="row">
                <div class="player-slider owl-carousel owl-theme">
                    @foreach($teams as $team)
                        <div class="item">
                            <div class="player-info">
                                <div class="player-image">
                                    <img src="{{$team->image}}" class="w-100" alt="">
                                </div>
                                <div class="player-desc">
                                    <div class="player-number">
                                        <span>{{$team->number}}</span>
                                    </div>
                                    <div class="player-name">
                                        <h3>{{$team->name}}</h3>
                                        <p>{{$team->role->name}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <section class="news-section pt-0">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="latest-news">
                        <div class="section-title">
                            <h2>Latest News</h2>
                        </div>
                    </div>
                    <div class="row">
                        @forelse($latestNews as $news)
                        <div class="col-md-6">
                            <div class="news-list">
                                <a href="{{route('frontend.news_detail', $news->id)}}">
                                    <img src="{{$news->image}}" class="w-100" alt="">
                                </a>
                                <h6><i class='bx bx-calendar'></i> {{ $news->created_at->format('d F, Y') }}</h6>
                                <a href="{{route('frontend.news_detail', $news->id)}}">
                                    <h2>{{$news->title}}</h2>
                                </a>
                                <p>{!! $news->description !!}
                                </p>
                            </div>
                        </div>
                        @empty
                            Latest news not available!
                        @endforelse
                        <div class="col-md-12">
                            <div class="view-all">
                                <a href="{{route('frontend.news')}}">View All News</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="section-title">
                        <h2>League Table</h2>
                    </div>
                    <div class="league-table">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>SN</th>
                                <th>Team</th>
                                <th>P</th>
                                <th>W</th>
                                <th>D</th>
                                <th>L</th>
                                <th>PTS</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $i = 1;
                            @endphp
                            @foreach($fixtureTables as $fixtureTable)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$fixtureTable->opponent_team->name ?? $siteSetting->name}}</td>
                                    <td>{{$fixtureTable['matches_played'] ?? 0}}</td>
                                    <td>{{$fixtureTable['wins'] ?? 0}}</td>
                                    <td>{{$fixtureTable['draws'] ?? 0}}</td>
                                    <td>{{$fixtureTable['losses'] ?? 0}}</td>
                                    <td>{{$fixtureTable['points'] ?? 0}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="section-title pt-3">
                        <h2>Our Sponsers</h2>
                    </div>
                    <div class="sponser">
                        <img src="assets/img/sponser.jpg" class="w-100" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- <section class="video-section" style="background-image: url('assets/img/video-bg.jpg');">
      <div class="container" style="margin-top: -13%;">
        <div style="--swiper-navigation-color: #fff; --swiper-pagination-color: #fff;" class="swiper mySwiper2">
          <div class="swiper-wrapper">
            <div class="swiper-slide">
              <div class="video-content">
                <div class="video-thumbnail">
                  <img src="assets/img/video/1.jpg" class="w-100" alt="">
                  <div class="pulses">
                    <button type="button" class="play-btn js-video-button">
                    <a data-fancybox href="https://www.youtube.com/embed/zpOULjyy-n8?rel=0"
                      allowfullscreen></iframe>
                      <i class='bx bx-play'></i>
                    </a>
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <div class="swiper-slide">
              <div class="video-content">
                <div class="video-thumbnail">
                  <img src="assets/img/video/2.jpg" class="w-100" alt="">
                  <div class="pulses">
                    <button type="button" class="play-btn js-video-button">
                    <a data-fancybox href="https://www.youtube.com/embed/zpOULjyy-n8?rel=0"
                      allowfullscreen></iframe>
                      <i class='bx bx-play'></i>
                    </a>
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <div class="swiper-slide">
              <div class="video-content">
                <div class="video-thumbnail">
                  <img src="assets/img/video/3.jpg" class="w-100" alt="">
                  <div class="pulses">
                    <button type="button" class="play-btn js-video-button">
                    <a data-fancybox href="https://www.youtube.com/embed/zpOULjyy-n8?rel=0"
                      allowfullscreen></iframe>
                      <i class='bx bx-play'></i>
                    </a>
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <div class="swiper-slide">
              <div class="video-content">
                <div class="video-thumbnail">
                  <img src="assets/img/video/4.jpg" class="w-100" alt="">
                  <div class="pulses">
                    <button type="button" class="play-btn js-video-button">
                    <a data-fancybox href="https://www.youtube.com/embed/zpOULjyy-n8?rel=0"
                      allowfullscreen></iframe>
                      <i class='bx bx-play'></i>
                    </a>
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <div class="swiper-slide">
              <div class="video-content">
                <div class="video-thumbnail">
                  <img src="assets/img/video/5.jpg" class="w-100" alt="">
                  <div class="pulses">
                    <button type="button" class="play-btn js-video-button">
                    <a data-fancybox href="https://www.youtube.com/embed/zpOULjyy-n8?rel=0"
                      allowfullscreen></iframe>
                      <i class='bx bx-play'></i>
                    </a>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="swiper-button-next"></div>
          <div class="swiper-button-prev"></div>
        </div>
        <div class="match-head">
          <h1>Recent Match Highlights</h1>
        </div>
        <div thumbsSlider="" class="swiper mySwiper thumbnail">
          <div class="swiper-wrapper">
            <div class="swiper-slide">
              <img src="assets/img/video/1.jpg" />
            </div>
            <div class="swiper-slide">
              <img src="assets/img/video/2.jpg" />
            </div>
            <div class="swiper-slide">
              <img src="assets/img/video/3.jpg" />
            </div>
            <div class="swiper-slide">
              <img src="assets/img/video/4.jpg" />
            </div>
            <div class="swiper-slide">
              <img src="assets/img/video/5.jpg" />
            </div>
          </div>
        </div>
      </div>
    </section> -->
{{--    <section class="product-section">--}}
{{--        <div class="container">--}}
{{--            <div class="row">--}}
{{--                <div class="col-md-12">--}}
{{--                    <div class="mb-3 position-relative text-end">--}}
{{--                        <a class="text-white" href="#">View All Product</a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-md-3">--}}
{{--                    <div class="product-info">--}}
{{--                        <div class="product-image">--}}
{{--                            <img src="assets/img/pro3.jpg" class="w-100" alt="">--}}
{{--                        </div>--}}
{{--                        <div class="product-desc">--}}
{{--                            <h2>Men Football Shoes Boots Predator</h2>--}}
{{--                            <p>$125.25</p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-md-3">--}}
{{--                    <div class="product-info">--}}
{{--                        <div class="product-image">--}}
{{--                            <img src="assets/img/pro1.jpg" class="w-100" alt="">--}}
{{--                        </div>--}}
{{--                        <div class="product-desc">--}}
{{--                            <h2>Alpha Goalkeeper Glove</h2>--}}
{{--                            <p>$125.25</p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-md-3">--}}
{{--                    <div class="product-info">--}}
{{--                        <div class="product-image">--}}
{{--                            <img src="assets/img/pro2.jpg" class="w-100" alt="">--}}
{{--                        </div>--}}
{{--                        <div class="product-desc">--}}
{{--                            <h2>Athletic Training Boots</h2>--}}
{{--                            <p>$125.25</p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-md-3">--}}
{{--                    <div class="product-info">--}}
{{--                        <div class="product-image">--}}
{{--                            <img src="assets/img/pro1.jpg" class="w-100" alt="">--}}
{{--                        </div>--}}
{{--                        <div class="product-desc">--}}
{{--                            <h2>Weather Grip Goalkeeper Gloves</h2>--}}
{{--                            <p>$125.25</p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}
    <section class="gallery-section pt-0 pb-0">
        <div class="row no-gutters">
            <div class="col-md-2">
                <div class="gallery-image">
                    <a href="{{asset('frontend/assets/img/gallery/1.jpg')}}" data-fancybox="gallery">
                        <img src="{{asset('frontend/assets/img/gallery/1.jpg')}}" alt="" />
                    </a>
                </div>
            </div>
            <div class="col-md-2">
                <div class="gallery-image">
                    <a href="{{asset('frontend/assets/img/gallery/2.jpg')}}" data-fancybox="gallery">
                        <img src="{{asset('frontend/assets/img/gallery/2.jpg')}}" alt="" />
                    </a>
                </div>
            </div>
            <div class="col-md-2">
                <div class="gallery-image">
                    <a href="{{asset('frontend/assets/img/gallery/3.jpg')}}" data-fancybox="gallery">
                        <img src="{{asset('frontend/assets/img/gallery/3.jpg')}}" alt="" />
                    </a>
                </div>
            </div>
            <div class="col-md-2">
                <div class="gallery-image">
                    <a href="{{asset('frontend/assets/img/gallery/4.jpg')}}" data-fancybox="gallery">
                        <img src="{{asset('frontend/assets/img/gallery/4.jpg')}}" alt="" />
                    </a>
                </div>
            </div>
            <div class="col-md-2">
                <div class="gallery-image">
                    <a href="{{asset('frontend/assets/img/gallery/5.jpg')}}" data-fancybox="gallery">
                        <img src="{{asset('frontend/assets/img/gallery/5.jpg')}}" alt="" />
                    </a>
                </div>
            </div>
            <div class="col-md-2">
                <div class="gallery-image">
                    <a href="{{asset('frontend/assets/img/gallery/1.jpg')}}" data-fancybox="gallery">
                        <img src="{{asset('frontend/assets/img/gallery/1.jpg')}}" alt="" />
                    </a>
                </div>
            </div>
        </div>
    </section>
@endsection
