<?php include('partials/header.php')  ?>

<section class="breadcrumbs" style="background-image: url('assets/img/breadcrumb-bg.jpg');">
    <div class="container">
        <ol>
            <li><a href="index.html">Home</a></li>
            <li>News & Updates</li>
        </ol>
        <h2>News & Updates</h2>
    </div>
</section>
<!-- End Breadcrumbs -->
<section class="news">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                        <a href="news-detail.php">
                            <div class="news-list">
                                <img src="assets/img/news/1.jpg" class="w-100" alt="" />
                                <h6><i class="bx bx-calendar"></i> 22 July 2022</h6>
                                <h2>Latest Point Table For The Premier League</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-12">
                        <a href="news-detail.php">
                            <div class="news-list">
                                <img src="assets/img/news/2.jpg" class="w-100" alt="" />
                                <h6><i class="bx bx-calendar"></i> 22 July 2022</h6>
                                <h2>Latest Point Table For The Premier League</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-12">
                        <a href="news-detail.php">
                            <div class="news-list">
                                <img src="assets/img/news/3.jpg" class="w-100" alt="" />
                                <h6><i class="bx bx-calendar"></i> 22 July 2022</h6>
                                <h2>Latest Point Table For The Premier League</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 news-side">
                <div class="section-title">
                    <h2>Latest News</h2>
                </div>
                <div class="news-sub media news-list mb-4">
                    <img src="assets/img/news/1.jpg" class="mr-3" alt="" />
                    <div class="media-body">
                        <h2 class="mt-0">Latest Point Table For The Premier League</h2>
                        <h6><i class="bx bx-calendar"></i> 22 July 2022</h6>
                    </div>
                </div>
                <div class="news-sub media news-list mb-4">
                    <img src="assets/img/news/2.jpg" class="mr-3" alt="" />
                    <div class="media-body">
                        <h2 class="mt-0">Latest Point Table For The Premier League</h2>
                        <h6><i class="bx bx-calendar"></i> 22 July 2022</h6>
                    </div>
                </div>
                <div class="news-sub media news-list mb-4">
                    <img src="assets/img/news/3.jpg" class="mr-3" alt="" />
                    <div class="media-body">
                        <h2 class="mt-0">Latest Point Table For The Premier League</h2>
                        <h6><i class="bx bx-calendar"></i> 22 July 2022</h6>
                    </div>
                </div>
                <div class="news-sub media news-list mb-4">
                    <img src="assets/img/news/4.jpg" class="mr-3" alt="" />
                    <div class="media-body">
                        <h2 class="mt-0">Latest Point Table For The Premier League</h2>
                        <h6><i class="bx bx-calendar"></i> 22 July 2022</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include('partials/footer.php')  ?>
