<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fixture_tables', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('opponent_team_id')->unique();
            $table->foreign('opponent_team_id')->references('id')->on('opponent_teams')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('matches_played');
            $table->integer('wins');
            $table->integer('draws');
            $table->integer('losses');
            $table->integer('points');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fixture_tables');
    }
};
