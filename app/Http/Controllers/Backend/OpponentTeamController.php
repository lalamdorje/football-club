<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\OpponentTeamRequest;
use App\Models\OpponentTeam;
use Illuminate\Http\Request;

class OpponentTeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $opponentTeams = OpponentTeam::all();
        return view('backend.opponent_team.index', compact('opponentTeams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.opponent_team.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OpponentTeamRequest $request)
    {
        $opponent = OpponentTeam::create($request->all());
        if ($request->hasFile('logo')){
            $opponent->addMedia($request->file('logo'))->toMediaCollection();
        }
        return redirect()->route('opponent_team.index')->withMsg('Your Opponent Team has been successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(OpponentTeam $opponentTeam)
    {
        return view('backend.opponent_team.create', compact('opponentTeam'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OpponentTeamRequest $request, OpponentTeam $opponentTeam)
    {
        if ($request->hasFile('logo')){
            $opponentTeam->clearMediaCollection();
            $opponentTeam->addMedia($request->file('logo'))->toMediaCollection();
        }
        $opponentTeam->update($request->all());
        return redirect()->route('opponent_team.index')->withMsg('Your Opponent Team has been successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(OpponentTeam $opponentTeam)
    {
        if ($opponentTeam->hasMedia()){
            $opponentTeam->clearMediaCollection();
        }
        $opponentTeam->delete();
    }
}
