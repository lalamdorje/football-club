<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\About;
use App\Models\FixtureTable;
use App\Models\ImageSlider;
use App\Models\MatchFixture;
use App\Models\PlayerRole;
use App\Models\SiteSetting;
use Illuminate\Http\Request;
use App\Models\Team;
use App\Models\News;

class HomeController extends Controller
{
    public function index(){
        $siteSetting = SiteSetting::first();
        $about = About::first();
        $teams = Team::all();
        $imageSliders = ImageSlider::all();
        $latestNews=News::where('status', 1)->latest()->take(4)->get();
        $matches = MatchFixture::latest()->orderBy('date' , 'desc')->where('status', 0)->get();
        $completedMatches = MatchFixture::latest()->where('status', 1)->take(3)->get();
        $fixtureTables = FixtureTable::orderBy('points', 'desc')->get();
        $ourTable[0] = (new MatchFixture())->ownStats();
        $fixtureTables =  collect($ourTable)->merge($fixtureTables)->sortByDesc('points');;
        return view('frontend.index', compact('teams', 'siteSetting', 'about', 'imageSliders', 'latestNews', 'matches', 'completedMatches', 'fixtureTables'));
    }

    public function team(){
       $roles = PlayerRole::orderBy('order', 'asc')->get();
        return view('frontend.team', compact('roles'));
    }

    public function about(){
        $about=About::first();
        return view('frontend.about', compact('about'));
    }
}
