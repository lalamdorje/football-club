<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Team extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;
    protected $fillable = ['name','number','player_role_id'];
    protected $appends = ['image'];

    public function getImageAttribute()
    {
        return $this->hasMedia() ? $this->getMedia()[0]->getFullUrl() : 'Team';
    }

    public function role(){
        return $this->belongsTo(PlayerRole::class, 'player_role_id', 'id');
    }
}
