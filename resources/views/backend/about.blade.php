@extends('backend.layouts.app')
@section('styles')
    <link rel="stylesheet" type="text/css" href="https://jeremyfagis.github.io/dropify/dist/css/dropify.min.css">
@endsection
@section('content')
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">About</span>
            <h3 class="page-title">About Setting</h3>
        </div>
    </div>
    <!-- End Page Header -->

    <form class="row m-0 add-new-post" action="{{route('about.store')}}" method="POST"
          enctype="multipart/form-data">
        @csrf
        @if(isset($edit))
            @method('POST')
        @endif
        <div class="col-lg-9 col-md-12">
            <!-- Add New Post Form -->
            <div class="card card-small mb-3">
                <div class="card-body">
                    <div class="col-md-12 col-sm-12 p-0 pl-3">
                        <div class="row col-md-12 mb-5">
                            <div class="col-md-4">
                                <label>Image<span class="required">*</span></label>
                            </div>
                            <div class="col-md-7 card p-3">
                                <input name="image" type="file" class="dropify" data-height="200"
                                       data-default-file="{{isset($edit) ? $edit->image : ''}}" accept="image/*"/>
                                @if($errors->first('image'))
                                    <div class="text text-danger">
                                        * {{$errors->first('image')}}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 p-0 pl-3">
                        <label for="title" class="">Title<span class="required">*</span> </label>
                        <input class="form-control form-control-lg mb-1" id="title" type="text" name="title"
                               value="{{old('title')?old('title'):(isset($edit)?$edit->title:'')}}"
                               placeholder="Input Name">
                        @if($errors->first('title'))
                            <div class="text text-danger ">
                                {{$errors->first('title')}}
                            </div>
                        @endif
                    </div>
                    <div class="col-md-12 col-sm-12 p-0 pl-3">
                        <label>Description<span class="required">*</span> </label>
                        @if($errors->first('description'))
                            <div class="text text-danger">
                                *{{$errors->first('description')}}
                            </div>
                        @endif
                        <textarea id="mytextarea"
                                  name="description">{!! isset($edit)?$edit->description:(old('description') ?? '') !!}
                        </textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-12">
            <!-- Post Overview -->
            <div class='card card-small mb-3'>
                <div class="card-header border-bottom">
                    <h6 class="m-0">Actions</h6>
                </div>
                <div class='card-body p-0'>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item p-3">
						<span class="d-flex mb-2">
							<i class="material-icons mr-1">flag</i>
							<strong class="mr-1">Status:</strong>@if(isset($edit))<span class="text text-success">Created </span>@else
                                <span class="text text-danger">Not Created</span> @endif
						</span>
                        </li>
                        <li class="list-group-item d-flex px-3">
                            @if(isset($edit))
                                <button type="submit" id="update" class="btn btn-sm btn-accent ml-auto">
                                    <i class="material-icons">file_copy</i> Update
                                </button>
                            @else
                                <button type="submit" id="save" class="btn btn-sm btn-accent ml-auto">
                                    <i class="material-icons">file_copy</i> Publish
                                </button>
                            @endif
                        </li>
                    </ul>
                </div>
            </div>
            <!-- / Post Overview -->
        </div>
    </form>
@endsection
@section('js')
    <script type="text/javascript" src="https://jeremyfagis.github.io/dropify/dist/js/dropify.min.js"></script>
    <script>
        $('.dropify').dropify();
    </script>
@endsection
