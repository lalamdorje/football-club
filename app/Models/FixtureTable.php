<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FixtureTable extends Model
{
    use HasFactory;
    protected $fillable = ['opponent_team_id', 'matches_played', 'wins', 'losses', 'draws', 'points'];

    public function opponent_team(){
        return $this->belongsTo(OpponentTeam::class, 'opponent_team_id', 'id');
    }
}
