<?php

namespace Database\Seeders;

use App\Models\ImageSlider;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ImageSliderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $imageSliders = [
            [
                'title' => 'Barcelona football club',
                'subtitle' => 'Barcelona football club',
            ],
            [
                'title' => 'Barcelona football club',
                'subtitle' => 'Barcelona football club',
            ],
        ];
        foreach ($imageSliders as $key => $imageSlider) {
            $Slider = ImageSlider::create($imageSlider);
            $Slider->addMediaFromUrl(asset('/frontend/image-slider/' . ($key + 1) . '.jpg'))->toMediaCollection();
        }
    }
}
