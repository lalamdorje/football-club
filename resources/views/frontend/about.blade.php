@extends('frontend.layouts.app')
@section('content')
    <section class="breadcrumbs" style="background-image: url('frontend/assets/img/breadcrumb-bg.jpg');">
    <div class="container">
        <ol>
            <li><a href="{{route('frontend.index')}}">Home</a></li>
            <li>About Page</li>
        </ol>
        <h2>About Page</h2>
    </div>
    </section>
    <!-- End Breadcrumbs -->
    <section class="about-section">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="section-title">
                        <h2>{{$about->title}}</h2>
                    </div>
                    <div class="about-main">
                        <p>
                            {!! $about->description !!}
                        </p>

                    </div>
                </div>
                <div class="col-md-7">
                    <div class="about-image">
                        <img src="{{$about->image}}" class="w-100" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
{{--    <section class="about-info-section">--}}
{{--        <div class="container">--}}
{{--            <div class="row">--}}
{{--                <div class="col-md-3">--}}
{{--                    <div class="info-about">--}}
{{--                        <img src="assets/img/about/1.jpg" class="w-100" alt="">--}}
{{--                        <h3>Championship</h3>--}}
{{--                        <p>Lorem ipsum dolor sit amet, cu his aliquam honestatis, mel ut putant verear consec tetuer. Eu sed diam ignota sanctus latine.</p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-md-3">--}}
{{--                    <div class="info-about">--}}
{{--                        <img src="assets/img/about/2.jpg" class="w-100" alt="">--}}
{{--                        <h3>Stadium</h3>--}}
{{--                        <p>Lorem ipsum dolor sit amet, cu his aliquam honestatis, mel ut putant verear consec tetuer. Eu sed diam ignota sanctus latine.</p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-md-3">--}}
{{--                    <div class="info-about">--}}
{{--                        <img src="assets/img/about/3.jpg" class="w-100" alt="">--}}
{{--                        <h3>Training</h3>--}}
{{--                        <p>Lorem ipsum dolor sit amet, cu his aliquam honestatis, mel ut putant verear consec tetuer. Eu sed diam ignota sanctus latine.</p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-md-3">--}}
{{--                    <div class="info-about">--}}
{{--                        <img src="assets/img/about/4.jpg" class="w-100" alt="">--}}
{{--                        <h3>Training</h3>--}}
{{--                        <p>Lorem ipsum dolor sit amet, cu his aliquam honestatis, mel ut putant verear consec tetuer. Eu sed diam ignota sanctus latine.</p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}

    <section class="gallery-section pt-0 pb-0">
        <div class="row no-gutters">
            <div class="col-md-2">
                <div class="gallery-image">
                    <a href="{{asset('frontend/assets/img/gallery/1.jpg')}}" data-fancybox="gallery">
                        <img src="{{asset('frontend/assets/img/gallery/1.jpg')}}" alt="" />
                    </a>
                </div>
            </div>
            <div class="col-md-2">
                <div class="gallery-image">
                    <a href="{{asset('frontend/assets/img/gallery/2.jpg')}}" data-fancybox="gallery">
                        <img src="{{asset('frontend/assets/img/gallery/2.jpg')}}" alt="" />
                    </a>
                </div>
            </div>
            <div class="col-md-2">
                <div class="gallery-image">
                    <a href="{{asset('frontend/assets/img/gallery/3.jpg')}}" data-fancybox="gallery">
                        <img src="{{asset('frontend/assets/img/gallery/3.jpg')}}" alt="" />
                    </a>
                </div>
            </div>
            <div class="col-md-2">
                <div class="gallery-image">
                    <a href="{{asset('frontend/assets/img/gallery/4.jpg')}}" data-fancybox="gallery">
                        <img src="{{asset('frontend/assets/img/gallery/4.jpg')}}" alt="" />
                    </a>
                </div>
            </div>
            <div class="col-md-2">
                <div class="gallery-image">
                    <a href="{{asset('frontend/assets/img/gallery/5.jpg')}}" data-fancybox="gallery">
                        <img src="{{asset('frontend/assets/img/gallery/5.jpg')}}" alt="" />
                    </a>
                </div>
            </div>
            <div class="col-md-2">
                <div class="gallery-image">
                    <a href="{{asset('frontend/assets/img/gallery/1.jpg')}}" data-fancybox="gallery">
                        <img src="{{asset('frontend/assets/img/gallery/1.jpg')}}" alt="" />
                    </a>
                </div>
            </div>
        </div>
    </section>
@endsection
