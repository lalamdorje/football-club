@extends('frontend.layouts.app')
@section('content')
    <section class="breadcrumbs" style="background-image: url('{{asset('frontend/assets/img/breadcrumb-bg.jpg')}}');">
        <div class="container">
            <ol>
                <li><a href="{{route('frontend.index')}}">Home</a></li>
                <li>Our Team</li>
            </ol>
            <h2>Our Team</h2>
        </div>
    </section>
    <section class="team-inner-section">
        <div class="container">
            @forelse($roles as $role)
                <div class="row mt-3">
                    <div class="section-title mb-2">
                        <h2>{{$role->name}}</h2>
                    </div>
                    @forelse($role->teams as $team)
                        <div class="col-md-3">
                            <div class="player-info">
                                <div class="player-image">
                                    <img src="{{ $team->image }}" class="w-100" alt=""/>
                                </div>
                                <div class="player-desc">
                                    <div class="player-number">
                                        <span>{{$team->number}}</span>
                                    </div>
                                    <div class="player-name">
                                        <h3>{{$team->name}}</h3>
                                        <p>{{$team->role->name}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @empty
                    @endforelse
                </div>
            @empty
            @endforelse
        </div>
    </section>
@endsection
