@extends('backend.layouts.app')
@section('content')
    <!-- Player Header -->
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">Match Fixture Section</span>
            <h3 class="page-title">Add New Match</h3>
        </div>
    </div>
    <!-- End Player Header -->
    <form class="add-new-post" action="{{isset($matchFixture)?route('match_fixture.update', $matchFixture):route('match_fixture.store')}}" method="post" enctype="multipart/form-data">
        <div class="row">
            <div class="col-lg-9 col-md-12">
                <!-- Add New Post Form -->
                <div class="card card-small mb-3">
                    <div class="card-body">
                        @csrf
                        @if(isset($matchFixture))
                            @method('PATCH')
                        @endif
                        <div class="col-md-12 col-sm-12 p-0 pl-3">
                            <label>Name<span class="required">*</span> </label>
                            <select class="form-control form-control-lg mb-3" id="opponent_team_id" name="opponent_team_id" >
                                <option value="">Select Opponent Team</option>
                                @foreach($opponentTeams as $opponentTeam)
                                    <option value="{{$opponentTeam->id}}" {{(isset($matchFixture) && $matchFixture->opponent_team_id == $opponentTeam->id) ? 'selected':''}}>{{$opponentTeam->name}}</option>
                                @endforeach
                            </select>
                            @if($errors->first('opponent_team_id'))
                                <div class="text text-danger">
                                    {{$errors->first('opponent_team_id')}}
                                </div>
                            @endif
                        </div>

                        <div class="col-md-12 col-sm-12 p-0 pl-3">
                            <label>Match Date<span class="required">*</span> </label>
                            <input class="form-control form-control-lg mb-3" id="date" type="date" name="date" value="{{old('date')?old('date'):(isset($matchFixture)?$matchFixture->date:'')}}" placeholder="Match Date">
                            @if($errors->first('date'))
                                <div class="text text-danger">
                                    {{$errors->first('date')}}
                                </div>
                            @endif
                        </div>

                    </div>
                </div>
                <!-- / Add New Post Form -->
            </div>
            <div class="col-lg-3 col-md-12">
                <!-- Post Overview -->
                <div class='card card-small mb-3'>
                    <div class="card-header border-bottom">
                        <h6 class="m-0">Actions</h6>
                    </div>
                    <div class='card-body p-0'>
                        <ul class="list-group list-group-flush">
{{--                            <li class="list-group-item p-3">--}}
{{--                                 <span class="d-flex">--}}
{{--                                    <div class="custom-control">--}}
{{--                                        <input type="hidden" name="status" value="0">--}}
{{--                                        <input type="checkbox" class="custom-control-input" id="customSwitch" name="status"--}}
{{--                                               value="1" {{ (isset($matchFixture) && $matchFixture->status == 1) || old('status') ? 'checked' : ''}}>--}}
{{--                                        <label class="custom-control-label" for="customSwitch">Is Active</label>--}}
{{--                                    </div>--}}
{{--                                </span>--}}
{{--                            </li>--}}
                            <li class="list-group-item d-flex px-3">
                                    <button type="submit" id="draft" class="btn btn-sm btn-outline-accent" name="status" value="draft">
                                        <i class="material-icons">save</i> Save Draft
                                    </button>
                                    <button type="submit" class="btn btn-sm btn-accent ml-auto">
                                        <i class="material-icons">file_copy</i> Publish
                                    </button>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- / Post Overview -->
            </div>
        </div>
    </form>
@endsection
@section('js')

@endsection
