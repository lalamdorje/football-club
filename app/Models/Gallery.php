<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Gallery extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $fillable = ['title'];

    protected $appends = ['image'];

    public function getImageAttribute(){
        return $this->hasMedia() ? $this->getMedia()[0]->getFullUrl() : 'Gallery Image';
    }
}
