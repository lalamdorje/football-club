<footer id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 footer-contact">
                    <h3>{{$siteSetting->name}}<span>.</span></h3>
                    <p>
                        {{$siteSetting->address}} <br>
                        <strong>Phone:</strong> {{$siteSetting->phone}}<br>
                        <strong class="mb-2" >Email:</strong> {{$siteSetting->email}}<br>
                        <br>
                        {!! $siteSetting->description !!}
                    </p>
                </div>
                <div class="col-lg-5 col-md-6 footer-links">
                    <h4>Useful Links</h4>
                    <ul>
                        <li><i class="bx bx-chevron-right"></i> <a href="{{route('frontend.index')}}">Home</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="{{route('frontend.about')}}">About us</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="{{route('frontend.team')}}">Teams</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="{{route('frontend.news')}}">News</a></li>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-6 footer-newsletter">
                    <h4>Join Our Newsletter</h4>
{{--                    <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>--}}
                    <form action="" method="post">
                        <input type="email" name="email"><input type="submit" value="Subscribe" placeholder="Your Email">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="container d-md-flex py-4">
        <div class="me-md-auto text-center text-md-start">
            <div class="copyright">
                &copy; Copyright <strong><span>{{$siteSetting->name}}</span></strong>. All Rights Reserved
            </div>
            <div class="credits">
                Designed by <a href="https://softmahal.com/" target="_blank" class="text-white">Softmahal Technologies</a>
            </div>
        </div>
        <div class="social-links text-center text-md-end pt-3 pt-md-0">
            <a href="{{$siteSetting->facebook_link}}" class="facebook"><i class="bx bxl-facebook"></i></a>
            <a href="{{$siteSetting->instagram_link}}" class="instagram"><i class="bx bxl-instagram"></i></a>
            <a href="{{$siteSetting->twitter_link}}" class="twitter"><i class="bx bxl-twitter"></i></a>
            <a href="{{$siteSetting->linkedin_link}}" class="linkedin"><i class="bx bxl-linkedin"></i></a>
        </div>
    </div>
</footer>
<!-- End Footer -->
<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
<!-- Vendor JS Files -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="{{asset('frontend/assets/vendor/aos/aos.js')}}"></script>
<script src="{{asset('frontend/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('frontend/assets/vendor/owl-carousel/owl.carousel.min.js')}}"></script>
<script src="{{asset('frontend/assets/vendor/glightbox/js/glightbox.min.js')}}"></script>
<script src="{{asset('frontend/assets/vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
<script type="text/javascript" src="{{asset('frontend/assets/vendor/fancybox/fancybox.min.js')}}"></script>
<script src="{{asset('frontend/assets/vendor/swiper/swiper-bundle.min.js')}}"></script>
<!-- Template Main JS File -->
<script src="{{asset('frontend/assets/js/main.js')}}"></script>
<script>
    var swiper = new Swiper(".mySwiper", {
        spaceBetween: 10,
        slidesPerView: 4,
        freeMode: true,
        watchSlidesProgress: true,
    });
    var swiper2 = new Swiper(".mySwiper2", {
        spaceBetween: 10,
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev"
        },
        thumbs: {
            swiper: swiper
        }
    });
</script>
@yield('js')
