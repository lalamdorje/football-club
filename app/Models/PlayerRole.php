<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlayerRole extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'order'];

    public function teams(){
        return $this->hasMany(Team::class);
    }
}
