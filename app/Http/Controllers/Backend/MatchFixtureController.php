<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\MatchRequest;
use App\Models\MatchFixture;
use App\Models\OpponentTeam;
use App\Models\SiteSetting;
use Illuminate\Http\Request;

class MatchFixtureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $siteSetting = SiteSetting::first();
        $opponentTeams = OpponentTeam::where('status', 1)->get();
        $matchFixtures = MatchFixture::latest()->get();
        return view('backend.match.index', compact('matchFixtures', 'opponentTeams', 'siteSetting'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $opponentTeams = OpponentTeam::where('status', 1)->get();
        return view('backend.match.create', compact('opponentTeams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(MatchRequest $request)
    {
        MatchFixture::create($request->all());
        return redirect()->route('match_fixture.index')->withMsg('Your match fixture has been successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(MatchFixture $matchFixture)
    {
        $opponentTeams = OpponentTeam::where('status', 1)->get();
        return view('backend.match.create', compact('matchFixture', 'opponentTeams'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(MatchRequest $request, MatchFixture $matchFixture)
    {
        $matchFixture->update($request->all());
        return redirect()->route('match_fixture.index')->withMsg('Your match fixture has been successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(MatchFixture $matchFixture)
    {
        $matchFixture->delete();
        return redirect()->route('match_fixture.index')->withMsg('Your match fixture has been successfully deleted.');
    }

    public function storeGoals(Request $request, $id)
    {
        $match = MatchFixture::find($id);
        $scored = $request->goal_scored;
        $conceded = $request->goal_conceded;
        $data = [
            'goal_scored' => $request->goal_scored,
            'goal_conceded' => $request->goal_conceded,
            'status' => 1,
        ];
        if ($scored > $conceded) {
            $data['result'] =  "wins";
        } elseif ($scored < $conceded) {
            $data['result'] =  "losses";
        } elseif ($scored == $conceded) {
            $data['result'] = "draws";
        } else {
            return abort(404, 'Matches Not Played');
        };
        $match->fill($data);
        $match->save();
        return response(json_encode($match));
    }
}
