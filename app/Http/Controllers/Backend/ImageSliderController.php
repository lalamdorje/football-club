<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\ImageSliderRequest;
use App\Models\ImageSlider;
use Illuminate\Http\Request;

class ImageSliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $imageSliders = ImageSlider::latest()->get();
        return view('backend.image_slider.index', compact('imageSliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.image_slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ImageSliderRequest $request)
    {
        $imageSlider = ImageSlider::create($request->all());
        if ($request->hasFile('image')){
            $imageSlider->addMedia($request->file('image'))->toMediaCollection();
        }
        return redirect()->route('image_slider.index')->withMsg('Your Image Slider has been successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ImageSlider $imageSlider)
    {
        return view('backend.image_slider.create', compact('imageSlider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ImageSliderRequest $request, ImageSlider $imageSlider)
    {
        if ($request->hasFile('image')){
            $imageSlider->clearMediaCollection();
            $imageSlider->addMedia($request->file('image'))->toMediaCollection();
        }
        $imageSlider->update($request->all());
        return redirect()->route('image_slider.index')->withMsg('Your Image Slider has been successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ImageSlider $imageSlider)
    {
        if ($imageSlider->hasMedia()){
            $imageSlider->clearMediaCollection();
        }
        $imageSlider->delete();
        return redirect()->route('image_slider.index')->withMsg('Your Image Slider has been successfully deleted.');
    }
}
