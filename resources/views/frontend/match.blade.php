@extends('frontend.layouts.app')
@section('content')
    <section class="breadcrumbs" style="background-image: url('frontend/assets/img/breadcrumb-bg.jpg');">
        <div class="container">
            <ol>
                <li><a href="index.html">Home</a></li>
                <li>Matches</li>
            </ol>
            <h2>Matches</h2>
        </div>
    </section>
    <!-- End Breadcrumbs -->
    <section class="match pt-0">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Fixtures</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">Results</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-contact" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">Tables</button>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                            <div class="row">
                                @forelse($matches as $match)
                                    <div class="col-md-6">
                                        <div class="last-match fixtures">
                                            <div class="club-logo">
                                                <img src="{{$siteSetting->logo}}" alt="" />
                                                <h4>{{$siteSetting->name}}</h4>
                                            </div>
                                            <div class="club-result">
                                                <p>{{date('d F, Y', strtotime($match->date))}}</p>
                                            </div>
                                            <div class="club-logo">
                                                <img src="{{$match->opponent_team->logo}}" alt="" />
                                                <h4>{{$match->opponent_team->name}}</h4>
                                            </div>
                                        </div>
                                    </div>
                                @empty
                                    <h5>
                                        <strong>There aren't any fixtures now.</strong>
                                    </h5>
                                @endforelse
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <div class="row">
                                @forelse($completedMatches as $completedMatch)
                                    <div class="col-md-6">
                                        <div class="last-match fixtures">
                                            <div class="club-logo">
                                                <img src="{{$siteSetting->logo}}" alt="" />
                                                <h4>{{$siteSetting->name}}</h4>
                                            </div>
                                            <div class="club-result">
                                                <p>
                                                    {{date('d F, Y', strtotime($completedMatch->date))}}
                                                </p>
                                                <h3>{{$completedMatch->goal_scored}} - {{$completedMatch->goal_conceded}}</h3>
                                            </div>
                                            <div class="club-logo">
                                                <img src="{{$completedMatch->opponent_team->logo}}" alt="" />
                                                <h4>{{$completedMatch->opponent_team->name}}</h4>
                                            </div>
                                        </div>
                                    </div>
                                @empty
                                    <h5>
                                        <strong>There aren't any completed matches.</strong>
                                    </h5>
                                @endforelse
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                            <div class="league-table">
                                <table class="table">
                                    <thead>
                                    <tr >
                                        <th>SN</th>
                                        <th>Team</th>
                                        <th>P</th>
                                        <th>W</th>
                                        <th>D</th>
                                        <th>L</th>
                                        <th>PTS</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                    $i = 1;
                                    @endphp

                                    @foreach($fixtureTables as $fixtureTable)

                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{$fixtureTable->opponent_team->name ?? $siteSetting->name}}</td>
                                            <td>{{$fixtureTable['matches_played'] ?? 0}}</td>
                                            <td>{{$fixtureTable['wins'] ?? 0}}</td>
                                            <td>{{$fixtureTable['draws'] ?? 0}}</td>
                                            <td>{{$fixtureTable['losses'] ?? 0}}</td>
                                            <td>{{$fixtureTable['points'] ?? 0}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 news-side mt-5">
                    <div class="section-title">
                        <h2>Latest News</h2>
                    </div>
                    @foreach($latestNews as $latestNew)
                        <div class="news-sub media news-list mb-4">
                        <img src="{{$latestNew->image}}" class="mr-3" alt="" />
                        <div class="media-body">
                            <h2 class="mt-0">{{$latestNew->title}}</h2>
                            <h6><i class="bx bx-calendar"></i> {{$latestNew->created_at->format('d F Y')}}</h6>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection
