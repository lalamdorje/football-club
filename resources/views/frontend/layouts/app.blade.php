<!DOCTYPE html>
<html lang="en">
<head>
    @include('frontend.layouts.header')
</head>
@php
    $siteSetting = \App\Models\SiteSetting::first();
@endphp
<body>
<!-- ======= Header ======= -->
<header id="header" class="fixed-top d-flex align-items-center">
    <div class="container d-flex align-items-center">
        <h1 class="logo me-auto"><a href="{{route('frontend.index')}}">{{$siteSetting->name}}<span>.</span></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html" class="logo me-auto"><img src="assets/img/logo.png" alt=""></a>-->
        <nav id="navbar" class="navbar order-last order-lg-0">
            <ul>
                <li><a class="nav-link {{ \Illuminate\Support\Facades\Request::is('/') ? 'active' : '' }}" href="{{route('frontend.index')}}">Home</a></li>
                <li><a class="nav-link {{ \Illuminate\Support\Facades\Request::is('about') ? 'active' : '' }}" href="{{route('frontend.about')}}">About</a></li>
                <li><a class="nav-link {{ \Illuminate\Support\Facades\Request::is('match') ? 'active' : '' }}" href="{{route('frontend.match')}}">Matches</a></li>
                <li><a class="nav-link {{ \Illuminate\Support\Facades\Request::is('team') ? 'active' : '' }}" href="{{route('frontend.team')}}">Team</a></li>
                <li><a class="nav-link {{ \Illuminate\Support\Facades\Request::is('news') ? 'active' : '' }}" href="{{route('frontend.news')}}">News & Updates</a></li>
                <li><a class="nav-link {{ \Illuminate\Support\Facades\Request::is('contact') ? 'active' : '' }}" href="{{route('frontend.contact')}}">Contact</a></li>
            </ul>
            <i class="bi bi-list mobile-nav-toggle"></i>
        </nav>
        <!-- .navbar -->
        <!-- <a href="#about" class="get-started-btn scrollto">Get Started</a> -->
    </div>
</header>
<!-- End Header -->
@yield('content')
@include('frontend.layouts.footer')
<!-- End #main -->
</body>
</html>

