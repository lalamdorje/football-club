<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\FixtureTableRequest;
use App\Models\FixtureTable;
use App\Models\MatchFixture;
use App\Models\OpponentTeam;
use App\Models\SiteSetting;
use Illuminate\Http\Request;

class FixtureTableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $siteSetting = SiteSetting::first();
        $fixtureTables = FixtureTable::orderBy('points', 'desc')->get();
        $ourTable[0] = (new MatchFixture())->ownStats();
        $fixtureTables =  collect($ourTable)->merge($fixtureTables)->sortByDesc('points');
        return view('backend.fixture_table.index', compact('fixtureTables', 'siteSetting'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $opponentTeams = OpponentTeam::where('status', 1)->get();
        return view('backend.fixture_table.create', compact('opponentTeams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FixtureTableRequest $request)
    {
        $request['matches_played'] = $request->wins + $request->draws + $request->losses;
        FixtureTable::create($request->all());
        return redirect()->route('fixture_table.index')->withMsg('Team added to fixture table successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(FixtureTable $fixtureTable)
    {
        $opponentTeams = OpponentTeam::where('status', 1)->get();
        return view('backend.fixture_table.create', compact('fixtureTable', 'opponentTeams'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FixtureTableRequest $request, FixtureTable $fixtureTable)
    {
        $fixtureTable->update($request->all());
        return redirect()->route('fixture_table.index')->withMsg('Team updated to fixture table successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(FixtureTable $fixtureTable)
    {
        $fixtureTable->delete();
        return redirect()->route('fixture_table.index')->withMsg('Team deleted from fixture table successfully');
    }
}
