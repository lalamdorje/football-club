<?php

namespace Database\Seeders;
use App\Models\News;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class NewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $newses=[
            [
                'title'=>'How will Barcelona line up aganist Viktoria Plzen in Champions League finale',
                'description'=>'Barcelona play the most meaningless game of the entire season on Tuesday evening when they travel to the Czech Republic to face Viktoria Plzen in the Champions League group stage finale.

                Barça’s loss to Bayern Munich last week sent them to the Europa League for the second straight season, but they’ll still hear the Champions League anthem one more time against one of the worst teams in the competition’s recent history.
                
                Plzen come home for their final game in Europe this season and will look for a famous result in front of their fans against one of the biggest clubs in the universe. Here’s how we think Barça will line up on Tuesday.',
                'status' => 1,
            ],
            [
                'title'=>'Barcelona striker offers himself to Jose Mourinho’s AS Roma – report',
                'description'=>'Memphis Depay could very well be on his way out of Barcelona when the January transfer window opens for business.

                The Netherlands international came extremely close to leaving Spotify Camp Nou in the summer, but a move to Juventus ended up collapsing. Having stayed out, the 28-year-old has found game-time extremely hard to come by this season.',
                'status' => 1,
            ],
            [
                'title'=>'Barcelona manager Xavi laments worst Champions League group stage in years',
                'description'=>'"We have nothing to play for, but this [Viktoria Plzen] is an important game," Xavi said. "We want to end this competition well.

                "We lack maturity. We have a lot of youngsters and we ve had the worst Champions League group in years. We ve had injuries. They are not excuses, but that has hurt us. With respect to last season, we have a better squad and we have given a better image in Europe. But we havet been up to the task."', 
                'status' => 1,
            ],
            [
                'title'=>'Barcelona striker offers himself to Jose Mourinho’s AS Roma – report',
                'description'=>'Memphis Depay could very well be on his way out of Barcelona when the January transfer window opens for business.

                The Netherlands international came extremely close to leaving Spotify Camp Nou in the summer, but a move to Juventus ended up collapsing. Having stayed out, the 28-year-old has found game-time extremely hard to come by this season.',
                'status' => 1,   
            ],
            ];
            foreach($newses as $key => $news){
                $t=News::create($news);
                $t->addMediaFromUrl(asset('/frontend/news/' . ($key + 1) . '.jpg'))->toMediaCollection();

            }
    }
}
