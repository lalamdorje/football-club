<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\PlayerRoleRequest;
use App\Models\PlayerRole;
use Illuminate\Http\Request;

class PlayerRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $playerRoles = PlayerRole::latest()->get();
        return view('backend.player_role.index', compact('playerRoles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.player_role.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PlayerRoleRequest $request)
    {
        PlayerRole::create($request->all());
        return redirect()->route('player_role.index')->withMsg('Your Player Role has been successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PlayerRole $playerRole)
    {
        return view('backend.player_role.create', compact('playerRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PlayerRoleRequest $request, PlayerRole $playerRole)
    {
        $playerRole->update($request->all());
        return redirect()->route('player_role.index')->withMsg('Your Player Role has been successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PlayerRole $playerRole)
    {
        $playerRole->delete();
        return redirect()->route('player_role.index')->withMsg('Your Player Role has been successfully deleted.');
    }
}
