<?php include('partials/header.php')  ?>

<section class="breadcrumbs" style="background-image: url('assets/img/breadcrumb-bg.jpg');">
    <div class="container">
        <ol>
            <li><a href="index.html">Home</a></li>
            <li>Matches</li>
        </ol>
        <h2>Matches</h2>
    </div>
</section>
<!-- End Breadcrumbs -->
<section class="match pt-0">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Fixtures</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">Results</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-contact" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">Tables</button>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="last-match fixtures">
                                    <div class="club-logo">
                                        <img src="assets/img/logo/1.png" alt="" />
                                        <h4>Manchester City</h4>
                                    </div>
                                    <div class="club-result">
                                        <p>July 23 , 2022</p>
                                        <p>Etihad Stadium</p>
                                    </div>
                                    <div class="club-logo">
                                        <img src="assets/img/logo/2.png" alt="" />
                                        <h4>Manchester United</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="last-match fixtures">
                                    <div class="club-logo">
                                        <img src="assets/img/logo/1.png" alt="" />
                                        <h4>Manchester City</h4>
                                    </div>
                                    <div class="club-result">
                                        <p>July 23 , 2022</p>
                                        <p>Etihad Stadium</p>
                                    </div>
                                    <div class="club-logo">
                                        <img src="assets/img/logo/2.png" alt="" />
                                        <h4>Manchester United</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="last-match fixtures">
                                    <div class="club-logo">
                                        <img src="assets/img/logo/1.png" alt="" />
                                        <h4>Manchester City</h4>
                                    </div>
                                    <div class="club-result">
                                        <p>July 23 , 2022</p>
                                        <p>Etihad Stadium</p>
                                    </div>
                                    <div class="club-logo">
                                        <img src="assets/img/logo/2.png" alt="" />
                                        <h4>Manchester United</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="last-match fixtures">
                                    <div class="club-logo">
                                        <img src="assets/img/logo/1.png" alt="" />
                                        <h4>Manchester City</h4>
                                    </div>
                                    <div class="club-result">
                                        <p>July 23 , 2022</p>
                                        <p>Etihad Stadium</p>
                                    </div>
                                    <div class="club-logo">
                                        <img src="assets/img/logo/2.png" alt="" />
                                        <h4>Manchester United</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="last-match fixtures">
                                    <div class="club-logo">
                                        <img src="assets/img/logo/1.png" alt="" />
                                        <h4>Manchester City</h4>
                                    </div>
                                    <div class="club-result">
                                        <p>July 23 , 2022</p>
                                        <h3>10 - 3</h3>
                                        <p>Etihad Stadium</p>
                                    </div>
                                    <div class="club-logo">
                                        <img src="assets/img/logo/2.png" alt="" />
                                        <h4>Manchester United</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="last-match fixtures">
                                    <div class="club-logo">
                                        <img src="assets/img/logo/1.png" alt="" />
                                        <h4>Manchester City</h4>
                                    </div>
                                    <div class="club-result">
                                        <p>July 23 , 2022</p>
                                        <h3>10 - 3</h3>
                                        <p>Etihad Stadium</p>
                                    </div>
                                    <div class="club-logo">
                                        <img src="assets/img/logo/2.png" alt="" />
                                        <h4>Manchester United</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="last-match fixtures">
                                    <div class="club-logo">
                                        <img src="assets/img/logo/1.png" alt="" />
                                        <h4>Manchester City</h4>
                                    </div>
                                    <div class="club-result">
                                        <p>July 23 , 2022</p>
                                        <h3>10 - 3</h3>
                                        <p>Etihad Stadium</p>
                                    </div>
                                    <div class="club-logo">
                                        <img src="assets/img/logo/2.png" alt="" />
                                        <h4>Manchester United</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="last-match fixtures">
                                    <div class="club-logo">
                                        <img src="assets/img/logo/1.png" alt="" />
                                        <h4>Manchester City</h4>
                                    </div>
                                    <div class="club-result">
                                        <p>July 23 , 2022</p>
                                        <h3>10 - 3</h3>
                                        <p>Etihad Stadium</p>
                                    </div>
                                    <div class="club-logo">
                                        <img src="assets/img/logo/2.png" alt="" />
                                        <h4>Manchester United</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <div class="league-table">
                  <table class="table">
                     <thead>
                        <tr >
                           <th>SN</th>
                           <th>Team</th>
                           <th>W</th>
                           <th>L</th>
                           <th>PTS</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <td>1</td>
                           <td>Barcelona</td>
                           <td>26</td>
                           <td>3</td>
                           <td>83</td>
                        </tr>
                        <tr>
                           <td>2</td>
                           <td>Arsenal</td>
                           <td>24</td>
                           <td>4</td>
                           <td>80</td>
                        </tr>
                        <tr>
                           <td>3</td>
                           <td>Man City</td>
                           <td>24</td>
                           <td>3</td>
                           <td>79</td>
                        </tr>
                        <tr>
                           <td>4</td>
                           <td>Manchester Unt</td>
                           <td>22</td>
                           <td>3</td>
                           <td>75</td>
                        </tr>
                        <tr>
                           <td>5</td>
                           <td>Liverpool</td>
                           <td>22</td>
                           <td>4</td>
                           <td>74</td>
                        </tr>
                        <tr>
                           <td>6</td>
                           <td>Chelsea</td>
                           <td>20</td>
                           <td>4</td>
                           <td>70</td>
                        </tr>
                        <tr>
                           <td>7</td>
                           <td>Atletico Madrid</td>
                           <td>18</td>
                           <td>6</td>
                           <td>64</td>
                        </tr>
                        <tr>
                           <td>8</td>
                           <td>Valencia</td>
                           <td>15</td>
                           <td>9</td>
                           <td>55</td>
                        </tr>
                        <tr>
                           <td>9</td>
                           <td>Real Sociedad</td>
                           <td>12</td>
                           <td>10</td>
                           <td>48</td>
                        </tr>
                        <tr>
                           <td>10</td>
                           <td>Real Madrid</td>
                           <td>12</td>
                           <td>12</td>
                           <td>48</td>
                        </tr>
                     </tbody>
                  </table>
               </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 news-side mt-5">
                <div class="section-title">
                    <h2>Latest News</h2>
                </div>
                <div class="news-sub media news-list mb-4">
                    <img src="assets/img/news/1.jpg" class="mr-3" alt="" />
                    <div class="media-body">
                        <h2 class="mt-0">Latest Point Table For The Premier League</h2>
                        <h6><i class="bx bx-calendar"></i> 22 July 2022</h6>
                    </div>
                </div>
                <div class="news-sub media news-list mb-4">
                    <img src="assets/img/news/2.jpg" class="mr-3" alt="" />
                    <div class="media-body">
                        <h2 class="mt-0">Latest Point Table For The Premier League</h2>
                        <h6><i class="bx bx-calendar"></i> 22 July 2022</h6>
                    </div>
                </div>
                <div class="news-sub media news-list mb-4">
                    <img src="assets/img/news/3.jpg" class="mr-3" alt="" />
                    <div class="media-body">
                        <h2 class="mt-0">Latest Point Table For The Premier League</h2>
                        <h6><i class="bx bx-calendar"></i> 22 July 2022</h6>
                    </div>
                </div>
                <div class="news-sub media news-list mb-4">
                    <img src="assets/img/news/4.jpg" class="mr-3" alt="" />
                    <div class="media-body">
                        <h2 class="mt-0">Latest Point Table For The Premier League</h2>
                        <h6><i class="bx bx-calendar"></i> 22 July 2022</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include('partials/footer.php')  ?>
