<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Contact;
use App\Models\SiteSetting;


class ContactController extends Controller
{
    public function contact(){
        $siteSetting = SiteSetting::first();
        return view('frontend.contact', compact('siteSetting'));
    }
    public function store(Request $request){
        Contact::create($request->all());
        return redirect()->back()->withMsg('Your Query has been sent successfully.');
    }
}
