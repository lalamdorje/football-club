<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class PlayerRoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
            'name' => 'required',
            'order' => 'required|unique:player_roles'
        ];
        if ($this->method() == 'PATCH' || $this->method() == 'PUT'){
            $rules['order'] = 'required';
        }
        return $rules;
    }
}
