<?php

namespace App\Http\Requests\Backend;


use Illuminate\Foundation\Http\FormRequest;

class NewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
            'title' => 'required',
            'description' => 'required',
            'image' => 'required|file|image:png,jpeg,jpg,gif',
        ];
        if ($this->method() == 'PATCH' || $this->method() == 'PUT')
        {
            $rules['image'] = 'nullable|file|image:png,jpeg,jpg,gif';
        }
        return $rules;
    }
}
