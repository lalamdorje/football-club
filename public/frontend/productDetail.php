<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta content="width=device-width, initial-scale=1.0" name="viewport">
      <title>Football Club</title>
      <meta content="" name="description">
      <meta content="" name="keywords">
      <!-- Favicons -->
      <link href="assets/img/favicon.png" rel="icon">
      <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
      <!-- Google Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
      <!-- Vendor CSS Files -->
      <link href="assets/vendor/aos/aos.css" rel="stylesheet">
      <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
      <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
      <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
      <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
      <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
      <link href="assets/vendor/fancybox/fancybox.min.css" rel="stylesheet">
      <link href="assets/vendor/owl-carousel/owl.carousel.min.css" rel="stylesheet">
      <link href="assets/vendor/owl-carousel/owl.theme.default.min.css" rel="stylesheet">
      <link href="assets/vendor/cloudzoom/cloudzoom.css" rel="stylesheet">
      <link href="assets/vendor/thumbelina/thumbelina.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
      <link href="assets/css/style.css" rel="stylesheet">
   </head>
   <body>
      <!-- ======= Header ======= -->
      <header id="header" class="fixed-top d-flex align-items-center">
         <div class="container d-flex align-items-center">
            <h1 class="logo me-auto"><a href="index.html">Football<span>.</span></a></h1>
            <nav id="navbar" class="navbar order-last order-lg-0">
               <ul>
                  <li><a class="nav-link active" href="#">Home</a></li>
                  <li><a class="nav-link" href="#">About</a></li>
                  <li><a class="nav-link" href="#">Services</a></li>
                  <li><a class="nav-link " href="#">Portfolio</a></li>
                  <li><a class="nav-link" href="#">Team</a></li>
                  <li><a href="blog.html">Blog</a></li>
                  <li class="dropdown">
                     <a href="#"><span>Drop Down</span> <i class="bi bi-chevron-down"></i></a>
                     <ul>
                        <li><a href="#">Drop Down 1</a></li>
                        <li class="dropdown">
                           <a href="#"><span>Deep Drop Down</span> <i class="bi bi-chevron-right"></i></a>
                           <ul>
                              <li><a href="#">Deep Drop Down 1</a></li>
                              <li><a href="#">Deep Drop Down 2</a></li>
                              <li><a href="#">Deep Drop Down 3</a></li>
                              <li><a href="#">Deep Drop Down 4</a></li>
                              <li><a href="#">Deep Drop Down 5</a></li>
                           </ul>
                        </li>
                        <li><a href="#">Drop Down 2</a></li>
                        <li><a href="#">Drop Down 3</a></li>
                        <li><a href="#">Drop Down 4</a></li>
                     </ul>
                  </li>
                  <li><a class="nav-link scrollto" href="#contact">Contact</a></li>
               </ul>
               <i class="bi bi-list mobile-nav-toggle"></i>
            </nav>
            <!-- .navbar -->
            <a href="#about" class="get-started-btn scrollto">Get Started</a>
         </div>
      </header>
      <section class="breadcrumbs" style="background-image: url('assets/img/breadcrumb-bg.jpg');">
         <div class="container">
            <ol>
               <li><a href="index.html">Home</a></li>
               <li>Product List Page</li>
            </ol>
            <h2>Product List Page</h2>
         </div>
      </section>
      <!-- End Breadcrumbs -->
      <section class="product-list-section">
         <div class="container">
            <div class="row">
               <div class="col-md-6">
                  <div id="surround">
                     <img class="cloudzoom" alt ="Cloud Zoom small image" id ="zoom1" src="assets/img/product/shoes.png"
                     data-cloudzoom='zoomSizeMode:"image",autoInside: 500'>
                     <div id="slider1">
                        <div class="thumbelina-but horiz left">&#706;</div>
                        <ul>
                           <li><img class='cloudzoom-gallery w-100' src="assets/img/product/shoes.png"
                           data-cloudzoom ="useZoom:'.cloudzoom', image:'assets/img/product/shoes.png' "></li>
                           <li><img class='cloudzoom-gallery w-100' src="https://assets-global.website-files.com/6027ab9296f259fcd4b6d69b/60a1f6ee56e0d8c7ee92f8fc_free-photos-for-blogs.jpeg"
                           data-cloudzoom ="useZoom:'.cloudzoom', image:'https://assets-global.website-files.com/6027ab9296f259fcd4b6d69b/60a1f6ee56e0d8c7ee92f8fc_free-photos-for-blogs.jpeg' "></li>
                           <li><img class='cloudzoom-gallery w-100' src="assets/img/about/2.jpg"
                           data-cloudzoom ="useZoom:'.cloudzoom', image:'assets/img/about/2.jpg' "></li>
                           <li><img class='cloudzoom-gallery w-100' src="assets/img/about/3.jpg"
                           data-cloudzoom ="useZoom:'.cloudzoom', image:'assets/img/about/3.jpg' "></li>
                           <li><img class='cloudzoom-gallery w-100' src="assets/img/about/4.jpg"
                           data-cloudzoom ="useZoom:'.cloudzoom', image:'assets/img/about/4.jpg' "></li>
                        </ul>
                        <div class="thumbelina-but horiz right">&#707;</div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="product-detail-info">
                     <h3>Men Football Shoes Boots Predator</h3>
                     <h5>$51.35</h5>
                     <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                     proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                     <ul>
                        <li><strong>Categories: </strong> <span>Equipment, Goalkeeper</span></li>
                        <li><strong>Product ID: </strong> <span>#254789</span></li>
                        <li><strong>Tags: </strong> <a href="#">Shoes</a> <a href="#">Boots</a> <a href="#">Mens</a></li>
                     </ul>
                     <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#buy-now-modal">
                     Buy Now
                     </button>
                  </div>
                  
               </div>
            </div>
         </div>
         <div class="modal fade" id="buy-now-modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header">
                     <h5 class="modal-title" id="exampleModalLabel">Football Shoes</h5>
                     <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div class="modal-body">
                     <h6>You can reach out to us to get more info on Football Shoes</h6>
                     <div class="buy-now-form">
                        <form>
                           <div class="mb-3">
                              <label for="fname" class="form-label">Full Name</label>
                              <input type="text" class="form-control" id="fname" placeholder="Your Name">
                           </div>
                           <div class="mb-3">
                              <label for="address" class="form-label">Address</label>
                              <input type="text" class="form-control" id="address" placeholder="Your Address">
                           </div>
                           <div class="mb-3">
                              <label for="email" class="form-label">Email Address</label>
                              <input type="email" class="form-control" id="email" placeholder="your@company.com">
                           </div>
                           <div class="mb-3">
                              <label for="phone" class="form-label">Phone Number</label>
                              <input type="text" class="form-control" id="phone" placeholder="+977-98xxxxxxxx">
                           </div>
                           <div class="mb-3">
                              <label for="message" class="form-label">Message</label>
                              <textarea class="form-control" id="message" rows="3"></textarea>
                           </div>
                        </form>
                     </div>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                     <button type="submit" class="btn btn-primary">Buy Now</button>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="product-section pt-0">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="section-title mb-4">
                     <h2>Similar Product</h2>
                  </div>
               </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="product-info">
                        <div class="product-image">
                           <img src="assets/img/product/1.jpg" class="w-100" alt="">
                        </div>
                        <div class="product-desc">
                          <h6>Shoes</h6>
                          <h2>Men Football Shoes Boots Predator</h2>
                          <p>$125.25</p>
                        </div>
                     </div>
              </div>
              <div class="col-md-3">
                <div class="product-info">
                        <div class="product-image">
                           <img src="assets/img/product/2.jpg" class="w-100" alt="">
                        </div>
                        <div class="product-desc">
                          <h6>Gloves</h6>
                          <h2>Alpha Goalkeeper Glove</h2>
                          <p>$125.25</p>
                        </div>
                     </div>
              </div>
              <div class="col-md-3">
                <div class="product-info">
                        <div class="product-image">
                           <img src="assets/img/product/3.jpg" class="w-100" alt="">
                        </div>
                        <div class="product-desc">
                          <h6>Shoes</h6>
                          <h2>Athletic Training Boots</h2>
                          <p>$125.25</p>
                        </div>
                     </div>
              </div>
              <div class="col-md-3">
                <div class="product-info">
                        <div class="product-image">
                           <img src="assets/img/product/4.jpg" class="w-100" alt="">
                        </div>
                        <div class="product-desc">
                          <h6>Gloves</h6>
                          <h2>Weather Grip Goalkeeper Gloves</h2>
                          <p>$125.25</p>
                        </div>
                     </div>
              </div>
              <div class="col-md-12">
                <div class="view-all mt-4">
                        <a href="#">View All Product</a>
                     </div>
              </div>
            </div>
         </div>
      </section>
      <!-- ======= Footer ======= -->
      <footer id="footer">
         <div class="footer-top">
            <div class="container">
               <div class="row">
                  <div class="col-lg-3 col-md-6 footer-contact">
                     <h3>Football<span>.</span></h3>
                     <p>
                        Devkota Sadak<br>
                        New Baneshwor, Kathmandu<br>
                        Nepal <br><br>
                        <strong>Phone:</strong> +977-9841234567<br>
                        <strong>Email:</strong> info@football.com<br>
                     </p>
                  </div>
                  <div class="col-lg-5 col-md-6 footer-links">
                     <h4>Useful Links</h4>
                     <ul>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
                     </ul>
                  </div>
                  <div class="col-lg-4 col-md-6 footer-newsletter">
                     <h4>Join Our Newsletter</h4>
                     <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
                     <form action="" method="post">
                        <input type="email" name="email"><input type="submit" value="Subscribe">
                     </form>
                  </div>
               </div>
            </div>
         </div>
         <div class="container d-md-flex py-4">
            <div class="me-md-auto text-center text-md-start">
               <div class="copyright">
                  &copy; Copyright <strong><span>Football</span></strong>. All Rights Reserved
               </div>
               <div class="credits">
                  Designed by <a href="https://softmahal.com/" target="_blank" class="text-white">Softmahal Technologies</a>
               </div>
            </div>
            <div class="social-links text-center text-md-end pt-3 pt-md-0">
               <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
               <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
               <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
               <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
               <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
            </div>
         </div>
      </footer>
      <!-- End Footer -->
      <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
      <!-- Vendor JS Files -->
      <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js"></script> -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
      <script src="assets/vendor/aos/aos.js"></script>
      <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      <script src="assets/vendor/owl-carousel/owl.carousel.min.js"></script>
      <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
      <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
      <script type="text/javascript" src="assets/vendor/fancybox/fancybox.min.js"></script>
      <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
      <script src="assets/vendor/cloudzoom/cloudzoom.js"></script>
      <script src="assets/vendor/thumbelina/thumbelina.js"></script>
      <script src="assets/js/main.js"></script>
      
   </body>
</html>