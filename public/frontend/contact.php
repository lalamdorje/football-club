<?php include('partials/header.php')  ?>

      <section class="breadcrumbs" style="background-image: url('assets/img/breadcrumb-bg.jpg');">
         <div class="container">
            <ol>
               <li><a href="index.html">Home</a></li>
               <li>Contact Page</li>
            </ol>
            <h2>Contact Page</h2>
         </div>
      </section>
      <!-- End Breadcrumbs -->
      <section class="contact-section">
         <div class="container">
            <div class="row">
               <div class="col-md-5">
                  <div class="get-in-touch">
                     <h3>Get in touch</h3>
                     <h5>We’ love to hear from you. Our friendly team is always here to chat.</h5>
                     <div class="contact-info">
                        <div class="media">
                           <i class='bx bx-envelope' ></i>
                           <div class="media-body">
                              <h6 class="mt-0">Chat to us</h6>
                              <p>Our friendly team is here to help.</p>
                              <p>football@gmail.com</p>
                           </div>
                        </div>
                     </div>
                     <div class="contact-info">
                        <div class="media">
                           <i class='bx bx-location-plus' ></i>
                           <div class="media-body">
                              <h6 class="mt-0">Office</h6>
                              <p>Devkota Sadak, New Baneshwor, Kathmandu, Nepal</p>
                           </div>
                        </div>
                     </div>
                     <div class="contact-info">
                        <div class="media">
                           <i class='bx bx-phone' ></i>
                           <div class="media-body">
                              <h6 class="mt-0">Phone</h6>
                              <p>Mon-Fri From 8AM-6PM</p>
                              <p>+977-9841234567, +977-98412312312</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-7">
                  <div class="contact-form">
                     <h3>Contact Us</h3>
                     <p>You can reach us anytime via football@gmail.com</p>
                     <form class="mt-4">
                       <div class="mb-3">
                         <label for="name" class="form-label">Full Name</label>
                         <input type="text" class="form-control" id="name" placeholder="Your Name">
                       </div>
                       <div class="mb-3">
                         <label for="email" class="form-label">Email Address</label>
                         <input type="email" class="form-control" id="email" placeholder="you@company.com">
                       </div>
                       <div class="mb-3">
                         <label for="phone" class="form-label">Phone Number</label>
                         <input type="text" class="form-control" id="phone" placeholder="+977-98xxxxxxxx">
                       </div>
                       <div class="mb-3">
                          <label for="message" class="form-label">How can we help?</label>
                          <textarea class="form-control" id="message" rows="4" placeholder="Tell us a about how can we help you.."></textarea>
                        </div>
                       
                       <button type="submit" class="btn btn-primary">Submit</button>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="map-section pb-0 pt-0">
         <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3532.708539331421!2d85.33510791490117!3d27.69540178279684!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb198a44e3c307%3A0x4e54cd2585eaab3c!2sSoftMahal%20Technologies%20Pvt.%20Ltd.!5e0!3m2!1sen!2snp!4v1655210328889!5m2!1sen!2snp" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
      </section>
      <?php include('partials/footer.php')  ?>
